package thread;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Slf4j
public class TestCallable {
    @Test
    public void test1() throws ExecutionException, InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(5);
        List<Future> list = new ArrayList<Future>();
        for (int i = 0; i < 10; i++) {
            CallableSample callableSample = new CallableSample(i, null);
            Future f = executor.submit(callableSample);
            f = executor.submit(callableSample);
            list.add(f);
        }
        executor.shutdown();
        for (Future f : list) {
            log.info("{}", f.get());
        }
    }

    @Test
    public void test2() throws ExecutionException, InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(5);
        ThreadLocal local = new ThreadLocal();
        List<Future> list = new ArrayList<Future>();
        for (int i = 0; i < 3; i++) {
            int finalI = i;
            CallableSample callableSample = new CallableSample(finalI, local);
            Future f = executor.submit(callableSample);
            list.add(f);
        }
        executor.shutdown();
        for (Future f : list) {
            log.info("{}", f.get());
        }
    }
    class BoundedBuffer {
        final Lock lock = new ReentrantLock();
        final Condition notFull  = lock.newCondition();
        final Condition notEmpty = lock.newCondition();

        final Object[] items = new Object[100];
        int putptr, takeptr, count;

        public void put(Object x) throws InterruptedException {
            lock.lock();
            try {
                while (count == items.length){
                    log.warn("waiting for take");
                    notFull.await();
                }
                items[putptr] = x;
                if (++putptr == items.length) putptr = 0;
                ++count;
                notEmpty.signal();
            } finally {
                lock.unlock();
            }
        }

        public Object take() throws InterruptedException {
            lock.lock();
            try {
                while (count == 0){
                    log.warn("waiting for add");
                    notEmpty.await();
                }

                Object x = items[takeptr];
                if (++takeptr == items.length) takeptr = 0;
                --count;
                notFull.signal();
                return x;
            } finally {
                lock.unlock();
            }
        }
    }

    @Test
    public void test3() {
        BoundedBuffer boundedBuffer=new BoundedBuffer();
        new Thread(()-> {
            try {
                while (true){
                    TimeUnit.MILLISECONDS.sleep(new Random().nextInt(50));
                    boundedBuffer.put(new Random().nextInt(1232131));
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        new Thread(()-> {
            try {
                while (true){
                    TimeUnit.MILLISECONDS.sleep(2*new Random().nextInt(50));
                    Object o=boundedBuffer.take();
                    log.info("taked:{}",o);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }).start();
        while (true){}
    }
}
