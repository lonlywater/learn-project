package spring;

import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import spring.bean.MathCalculate;
import spring.component.BookService;
import spring.config.ConfigOfAOP;
import spring.config.ConfigOfTx;

public class TestTx {
    /**
     * @EnableTransactionManagement
     *    使用 @Import TransactionManagementConfigurationSelector.class
     *       目的是为了导入两个组件
     *          AutoProxyRegistrar.class
     *             注册一个InfrastructureAdvisorAutoProxyCreator 的bean。这是一个beanPostProcessor后置处理器，在初始化后postProcessAfterInitialization，如果该方法有切面(事务)，就创建代理
     *          ProxyTransactionManagementConfiguration.class
     *             注入bean TransactionInterceptor:
     *                 负责拦截事务
     */
    @Test
    public void test1(){
        AbstractApplicationContext applicationContext = new AnnotationConfigApplicationContext(ConfigOfTx.class);
        for (String beanDefinitionName : applicationContext.getBeanDefinitionNames()) {
            System.out.println(beanDefinitionName+"-"+applicationContext.getBean(beanDefinitionName).getClass().getSimpleName());
        }
        BookService bookService = applicationContext.getBean(BookService.class);
        bookService.addRecord();
        applicationContext.close();
    }
    @Test
    public void test2(){
        AbstractApplicationContext applicationContext = new AnnotationConfigApplicationContext(ConfigOfTx.class);

        BookService bookService = applicationContext.getBean(BookService.class);
        bookService.addRecord2();
        applicationContext.close();
    }
}
