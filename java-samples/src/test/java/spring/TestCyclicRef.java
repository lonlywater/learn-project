package spring;

import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import spring.bean.MathCalculate;
import spring.config.ConfigCircle1;
import spring.config.ConfigOfCyclicRef;
import spring.cyclicRef.A;
import spring.cyclicRef.B;

public class TestCyclicRef {
    @Test
    public void test(){
        AbstractApplicationContext applicationContext = new AnnotationConfigApplicationContext(ConfigOfCyclicRef.class);
        for (String beanDefinitionName : applicationContext.getBeanDefinitionNames()) {
            System.out.println(beanDefinitionName);
        }
        //5.2版本，为什么Aspect注入以后 IDEA调试@Autowired 字段为空？？
        A a = (A) applicationContext.getBean("a");
        //内部调用可以，外部访问NPE??
        a.test();
//        a.b.getA().test();//NPE? why
        B b = (B) applicationContext.getBean("b");
        b.test();
        applicationContext.close();;
    }
    @Test
    /**
     * 构造注入，无法解决循环依赖
     */
    public void test2(){
        AbstractApplicationContext applicationContext = new AnnotationConfigApplicationContext(ConfigCircle1.class);
        for (String beanDefinitionName : applicationContext.getBeanDefinitionNames()) {
            System.out.println(beanDefinitionName);
        }
        applicationContext.close();;
    }
}
