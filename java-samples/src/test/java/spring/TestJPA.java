package spring;

import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import spring.jpa.ApplicationConfig;
import spring.jpa.bean.User;
import spring.jpa.hibernate.MyRepo;

public class TestJPA {
    @Test
    public void test1(){
        AbstractApplicationContext applicationContext = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        for (String beanDefinitionName : applicationContext.getBeanDefinitionNames()) {
            System.out.println(beanDefinitionName);
        }
        MyRepo myRepo= (MyRepo) applicationContext.getBean("myRepo");
        myRepo.save(new User());
    }
}
