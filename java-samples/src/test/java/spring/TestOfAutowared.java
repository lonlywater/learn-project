package spring;

import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import spring.component.BookDao;
import spring.component.BookService;
import spring.config.ConfigOfAutowared;

public class TestOfAutowared {
    @Test
    /**测试
     @Autowared
     @Qualifier
     */
    public void test1(){
        AbstractApplicationContext applicationContext = new AnnotationConfigApplicationContext(ConfigOfAutowared.class);
        for (String beanDefinitionName : applicationContext.getBeanDefinitionNames()) {
            System.out.println(beanDefinitionName);
        }
//        applicationContext.getEnvironment().setActiveProfiles();
        BookService bookService = (BookService) applicationContext.getBean("bookService");
        BookDao bookDao = (BookDao) applicationContext.getBean("bookDao");
        BookDao dao2 = (BookDao) applicationContext.getBean("dao2");
        System.out.println(bookService);
        System.out.println(bookDao);
        System.out.println(dao2);
    }
}
