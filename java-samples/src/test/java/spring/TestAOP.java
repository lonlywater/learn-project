package spring;

import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import spring.bean.MathCalculate;
import spring.component.BookDao;
import spring.component.BookService;
import spring.config.ConfigOfAOP;
import spring.config.ConfigOfAutowared;

public class TestAOP {
    /**
     * AbstractApplicationContext applicationContext = new AnnotationConfigApplicationContext(ConfigOfAOP.class);
     * 调用流程：
     * 1）、传入配置类创建IOC容器，调用一个有参构造器
     * 2）、注册配置类，调用refresh()刷新容器
     * 3）、invokeBeanFactoryPostProcessors注册调用内置的bean工厂类，然后registerBeanPostProcessors;创建注册：BeanPostProcessor
     *       1.获取容器中所有的需要创建的BeanPostProcessor，
     *       2.根据定义创建bean
     *       3.先PriorityOrder的BeanPostProcessor, 再Order的BeanPostProcessor以及最后普通的BeanPostProcessor
     *       4.注册BeanPostProcessor,实际就是创建，并放入容器中
     *             1.创建实例->单例且允许循环依赖的创建中的bean，是缓存了一个factoryBean，延后了生成bean的时间(addSingletonFactory)
     *             2.给属性赋值populateBean(beanName, mbd, instanceWrapper);
     *             3.初始化bean,initializeBean
     *                >先处理Aware系列的接口的回调，invokeAwareMethods
     *                >再处理初始化前。applyBeanPostProcessorsBeforeInitialization，
     *                   # 如果这里，某个Processor返回了null，后续的processor会被跳过
     *                   # InitDestroyAnnotationBeanPostProcessor也在这里执行，会处理 @PostConstruct的初始化
     *                >再调用初始化方法invokeInitMethods
     *                >处理初始化后，applyBeanPostProcessorsAfterInitialization
     *             4.如果有必要，则将bean注册到销毁的集合中
     *             5.BeanPostProcessor【AnnotationAwareAspectJAutoProxyCreator】创建成功-->aspectJAdvisorsBuilder
     *       5.把BeanPostProcessor注册到beanFactory中，registerBeanPostProcessors
     * 4)、初始化剩下的Bean,finishBeanFactoryInitialization，基于定义对所有的未创建的bean都创建出来
     *      1.遍历获取所有的bean，依次创建对象
     *         getBean-->doGetBean-->getSingleton->getObject->createBean
     *      2.创建bean(createBean)
     *         1)、resolveBeforeInstantiation，先执行InstantiationAwareBeanPostProcessor的beforeInstantiation，如果这里返回了一个自定义的bean，那么立马初始化执行afterInitialization（比如AOP)
     *         2） 没有提前创建的bean,doCreateBean
     *             0）、创建实例(这里涉及如何实例化，构造器推断等内容，)
     *             1）、MergedBeanDefinitionPostProcessor，执行这类接口，默认就是@Autowared 和@Value，@Inject的 方法&属性 进行扫描封装，用于后续做赋值
     *                1.这里执行完的时候，会添加factoryBean的缓存（调用addSingletonFactory），获取的时候，调用的是getEarlyBeanReference，这里会判断，如果需要AOP的话，这里会创建代理
     *             2）、populateBean，对属性进行赋值操作
     *                1）、InstantiationAwareBeanPostProcessor.postProcessAfterInstantiation，实例化后的拦截器，在这里，用户可以自定义修改bean（配合before方法），并return false来跳过spring的字段和方法注入
     *                2）、赋值(这里涉及postProcess其他的一些property相关的方法。)
     *                3）、这里实际处理了@Autowired, @Inject,@Resource之类的赋值，用的是在4).2.1)步骤中扫描出来的meta data，
     *                    这里如果存在循环依赖，如A->B->A，那么在B的populateBean中，会把A的从 singletonFactories 的缓存中拿出来，放入 earlySingletonObjects 缓存中，
     *                       B是直接从 singletonFactories 缓存--> singletonObjects 的缓存，A则是 singletonFactories 缓存--> earlySingletonObjects -->singletonObjects
     *             3）、initializeBean，初始化bean
     *                1）、invokeAwareMethods，对Aware系列的接口进行回调处理
     *                2）、applyBeanPostProcessorsBeforeInitialization，执行初始化前的BeanPostProcessor
     *                    >>InitDestroyAnnotationBeanPostProcessor  在这里执行，处理@PostConstruct
     *                3)、invokeInitMethods，初始化方法的调用，完成初始化
     *                    >>先执行org.springframework.beans.factory.InitializingBean.afterPropertiesSet
     *                    >>再执行Custom的初始化方法，即是：@Bean的方法
     *                4）、applyBeanPostProcessorsAfterInitialization。初始化后
     *             4)、registerDisposableBeanIfNecessary，注册为需要销毁的bean
     *                >>这里是使用DisposableBeanAdapter对bean的销毁方法做了适配
     *                >>要处理的是所有的DestructionAwareBeanPostProcessor实现的post processor
     *      3.如果是新创建的bean，addSingleton。在 getSingleton 方法中，将该方法注册到容器中，并删除2/3级的缓存
     */
    @Test
    public void test1(){
        AbstractApplicationContext applicationContext = new AnnotationConfigApplicationContext(ConfigOfAOP.class);
        for (String beanDefinitionName : applicationContext.getBeanDefinitionNames()) {
            System.out.println(beanDefinitionName);
        }
//        applicationContext.getEnvironment().setActiveProfiles();
        MathCalculate bookService = (MathCalculate) applicationContext.getBean("mathCalculate");
//        bookService.div(2,0);
        bookService.div(2,1);
        applicationContext.close();
    }
}
