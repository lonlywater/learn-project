package spring;

import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import spring.config.Config1;

public class TestBean {
    AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(Config1.class);
    @Test
    public void test1(){
        for (String beanName:applicationContext.getBeanDefinitionNames()){
            System.out.println(beanName);
        }
        applicationContext.close();
    }
    @Test
    public void test2(){
        applicationContext.close();
    }
}
