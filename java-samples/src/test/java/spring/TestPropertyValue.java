package spring;

import lombok.val;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import spring.bean.Person;
import spring.config.ConfigOfPropertySourcce;

public class TestPropertyValue {
    @Test
    public void test1(){
        AbstractApplicationContext applicationContext = new AnnotationConfigApplicationContext(ConfigOfPropertySourcce.class);
        for (String beanDefinitionName : applicationContext.getBeanDefinitionNames()) {
            System.out.println(beanDefinitionName);
        }
         Person person = (Person) applicationContext.getBean("person");
        System.out.println(person);
    }
}
