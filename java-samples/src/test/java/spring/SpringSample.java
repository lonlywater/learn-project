package spring;

import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.junit.Test;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

@Slf4j
public class SpringSample {

    @Test
    /**
     * BeanFactory就是容器，
     */
    public void beanFactoryTest(){

//        ClassPathXmlApplicationContext classPathXmlApplicationContext = new ClassPathXmlApplicationContext("spring.xml");
//        FileSystemXmlApplicationContext fileSystemXmlApplicationContext = new FileSystemXmlApplicationContext("");
        AnnotationConfigApplicationContext configApplicationContext = new AnnotationConfigApplicationContext();

        DefaultListableBeanFactory beanFactory = new DefaultListableBeanFactory();
        beanFactory.registerSingleton("user",new User());
        User user = beanFactory.getBean("user", User.class);//生成，但是未注册
        log.info("user:{}",user);


    }
}
