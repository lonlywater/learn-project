package springboot.redis;

import com.alibaba.fastjson.JSON;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class CacheService {
    @Resource
    RedisTemplate<String, Object> redisTemplate;

    private List<String> ids = null;

    public Object test1() {
        String key1 = "key1";
        redisTemplate.opsForValue().setIfAbsent(key1, 1);

        return redisTemplate.opsForValue().increment(key1);

    }

    public boolean lock(String key) {
        return redisTemplate.opsForValue().setIfAbsent(key, LOCK_ID, Duration.ofSeconds(10));
    }

    public static final String UNLOCK_SCRIPT =
            "if redis.call('get', KEYS[1]) == ARGV[1] " +
                    "then return redis.call('del', KEYS[1]) " +
                    "else return 0 end";
    public static final String LOCK_ID = UUID.randomUUID().toString();

    public Long unlock(String key) {
        RedisScript<Long> redisScript = new DefaultRedisScript<>(UNLOCK_SCRIPT, Long.class);
        return redisTemplate.execute(redisScript, Collections.singletonList(key), LOCK_ID);
    }

    public void initCache() {
        List<String> list = new ArrayList<>(1000);
        for (int i = 0; i < 1000; i++) {
            list.add(UUID.randomUUID().toString());
        }
        LocalDate date = LocalDate.of(2023, 3, 1);

        List<String> morning = Arrays.asList("张三", "李四", "王五", "马六", "黑七");
        List<String> afternoon = Arrays.asList("张1", "李2", "王3", "马4", "黑5");
        List<String> evening = Arrays.asList("张9", "李8", "王7", "马六", "黑5");

        for (int i = 0; i < list.size(); i++) {
            String id = list.get(i);
            Map<LocalDate, Object> hash = new HashMap<>();
            LocalDate today = date;
            while (today.isBefore(LocalDate.of(2023, 4, 1))) {
                hash.put(today, new Schedule(3, morning, afternoon, evening));
                today = today.plusDays(1l);
            }
            redisTemplate.opsForHash().putAll(id, hash);

        }
        ids = list;
    }

    public List<String> getIds() {
        return ids;
    }

    public Map<String, Schedule> querySchedule(List<String> ids, LocalDate date) {
        if (ids == null) {
            return null;
        }
        HashMap<String, Schedule> localDateScheduleHashMap = new HashMap<>();
        for (int i = 0; i < ids.size(); i++) {
            Schedule o = (Schedule) redisTemplate.opsForHash().get(ids.get(i), date);
            localDateScheduleHashMap.put(ids.get(i), o);
        }
        return localDateScheduleHashMap;
    }
}
