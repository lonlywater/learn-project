package springboot.redis;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Schedule {
    private Integer scheduleCnt;
    private List<String> morning;
    private List<String> afternoon;
    private List<String> evening;
}
