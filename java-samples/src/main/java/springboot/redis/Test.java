package springboot.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.Map;
import java.util.UUID;

@SpringBootApplication
@RestController
public class Test {
    public static void main(String[] args) {
        SpringApplication.run(Test.class, args);

    }
    @Resource
    CacheService cacheService;

    @GetMapping("test1")
    public String test1(){
        return "hello:incream:"+cacheService.test1();
    }
    @GetMapping("lock")
    public String lock(String key){
        if (key == null) {
            key = UUID.randomUUID().toString();
        }

        return String.valueOf(cacheService.lock(key));
    }
    @GetMapping("unlock")
    public String unlock(String key){
        return cacheService.unlock(key).toString();
    }
    @GetMapping("hash")
    public ResponseEntity hash(){
        if (cacheService.getIds()==null){
            cacheService.initCache();
            System.out.println(cacheService.getIds());
        }
        LocalDate of = LocalDate.of(2023, 3, 21);
        StopWatch watch = new StopWatch();
        watch.start("querySchedule");
        Map<String, Schedule> localDateScheduleMap = cacheService.querySchedule(cacheService.getIds(), of);
        watch.stop();
        System.out.println(watch.prettyPrint());
        return ResponseEntity.ok(localDateScheduleMap);
    }
}
