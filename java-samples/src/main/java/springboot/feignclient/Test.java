package springboot.feignclient;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @Author: Administrator
 * @Date: 2023/4/13 13:08
 */

@SpringBootApplication
@EnableFeignClients
@RestController

public class Test {

    @Autowired
    private  Feighclient feighclient;

    public static void main(String[] args) {
        SpringApplication.run(Test.class, args);

    }

    @GetMapping("/test")
    public Object hello(){
        return feighclient.getGrandLotto(85, 0, 10000, 1, 1);
    }
}

