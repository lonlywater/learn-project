package springboot.feignclient.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.StringHttpMessageConverter;

import javax.annotation.PostConstruct;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Configuration
@Slf4j
public class HttpMessageConvertConfiguration{

    @Autowired
    StringHttpMessageConverter converter;

    @PostConstruct
    public void test(){
        List list=new ArrayList();
        list.addAll(converter.getSupportedMediaTypes());
        list.add(MediaType.TEXT_HTML);
//        list.add(MediaType.TEXT_PLAIN);
        MediaType mediaType =new MediaType(MediaType.TEXT_PLAIN, StandardCharsets.UTF_8);
        log.info(mediaType.toString());
        list.add(mediaType);
        converter.setSupportedMediaTypes(list);
        log.info("sdfsf");
    }

}
