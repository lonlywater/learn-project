package springboot.feignclient;

/**
 * @Author: Administrator
 * @Date: 2023/4/13 13:07
 */

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;
@FeignClient(name = "GrandLotto", url = "https://webapi.sporttery.cn")
public interface Feighclient {
    @GetMapping(value = "/gateway/lottery/getHistoryPageListV1.qry", /*consumes="application/json",*/produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Object> getGrandLotto(@RequestParam Integer gameNo, @RequestParam Integer provinceId, @RequestParam Integer pageSize, @RequestParam Integer isVerify, @RequestParam Integer pageNo);
}

