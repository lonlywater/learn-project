package rabbitmq.A01hello;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Send {
    private static final String QUEUE_NAME = "noidler_queue1";

    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("192.168.192.132");
        connectionFactory.setUsername("admin");
        connectionFactory.setPassword("123456");
        try (Connection connection = connectionFactory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);
            String message = "Noidler:Hello World![%d]";
            int curr = 1;
            while (true) {
                TimeUnit.MILLISECONDS.sleep(100);
                channel.basicPublish("", QUEUE_NAME, null, String.format(message,curr++).getBytes());
//                System.out.println(" [Noidler] Sent '" + message + "'");
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
