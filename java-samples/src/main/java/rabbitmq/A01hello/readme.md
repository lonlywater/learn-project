##hello world demo
此demo展示 rabbitmq官网示例中的第一种。
![](img.png)
###特点
1.发送端：  
申明队列-->发送消息  
```
channel.queueDeclare(QUEUE_NAME, false, false, false, null);
channel.basicPublish("", QUEUE_NAME, null, String.format(message,curr++).getBytes());
```
  
2.接收者  
申明队列-->接收消息
```
//申明队列
channel.queueDeclare(QUEUE_NAME, false, false, false, null);  
//定义接收处理行为
DeliverCallback deliverCallback = (s, delivery) -> {
String message = new String(delivery.getBody(), "UTF-8");
System.out.println(" [x] Received '" + message + "'");
};
//等待消息
channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> {
        });
```