package rabbitmq;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RabbitMQUtil {
    private static ConnectionFactory connectionFactory = new ConnectionFactory();
    static {
        connectionFactory.setHost("192.168.1.100");
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
    }

    public static Connection newConnection() throws IOException, TimeoutException {

        return connectionFactory.newConnection();
    }

    public static Channel newChannel(Connection connection) throws IOException {
        return connection.createChannel();
    }

    public static Channel newChannel() throws IOException, TimeoutException {
        return newChannel(newConnection());
    }

}
