#Exchage -- Topic demo
本demo练习了 exchange type 为 topic类型的rabbit 工作模式  

###特点
1. exchange_type= topic
2. routingkey可以使用dots分割，例如a.v.c
3. bindingkey可以使用*、#表示匹配，  
   *：匹配一个单词，  
   \#：匹配0个或者多个单词
   
4. topic 类型的工作模式，可以覆盖之前学到的direct模式以及fanout模式，通过使用#或者不使用#/*

###测试的例子
发送者发送[even/odd].[INFO/WARNING/ERR].message-[\d]类型的消息  
通过修改参数，接收者启动三个实例：
![img_3.png](img_3.png)
1. 以参数even.*启动
   ![img.png](img.png)
2. 以参数*.ERR启动
   ![img_1.png](img_1.png)
3. 以参数#启动
   ![img_2.png](img_2.png)