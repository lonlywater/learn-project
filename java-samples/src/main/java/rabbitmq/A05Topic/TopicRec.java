package rabbitmq.A05Topic;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import rabbitmq.RabbitMQUtil;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * 启动参数：main参数：
 * 1.  even.*  表示接收even类型的消息
 * 2.  *.ERR   表示接收ERR类型的消息
 * 3.  #       表示接收全部类型
 */
public class TopicRec {
    private static String exchangeDeclare = "logs3";

    public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
        Connection connection = RabbitMQUtil.newConnection();
        Channel channel = RabbitMQUtil.newChannel(connection);
        channel.exchangeDeclare(exchangeDeclare, BuiltinExchangeType.TOPIC);
        String queueName = channel.queueDeclare().getQueue();
        //根据入参绑定key
        for (String str :
                args) {
            channel.queueBind(queueName, exchangeDeclare, str);
        }
        channel.basicConsume(queueName, true, "", (consumerTag, message) -> {
            String msg= new String(message.getBody(),"UTF-8");
            System.out.println(msg);
        }, consumerTag -> {

        });
    }
}
