#Publish & Subscribe
此demo展示 rabbitmq官网示例中的第3种。发布--订阅模式    
  
即：com.rabbitmq.client.BuiltinExchangeType.FANOUT  
这种模式跟第二种work queue显著的区别在于：  
work queue 想要做的是把一条消息分发给1个worker，  
pub & sub  则是要把一条消息发给多个worker  
![img_8.png](img_8.png)
###特点
1.发送端：  
申明交换机-->发送消息  
```
channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT);
channel.basicPublish(EXCHANGE_NAME, "", null, String.format(message,curr++).getBytes());
```
  
2.接收者  
申明队列-->接收消息
```
//申明交换器
channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
//create a non-durable, exclusive, autodelete queue with a generated name
//申明一个服务器命名的(随机)队列，这个队列，属于临时队列，auto-delete, exclusive, un-durable
String queueName = channel.queueDeclare().getQueue();
//与交换机绑定
channel.queueBind(queueName, EXCHANGE_NAME, "");
//定义接收处理行为
DeliverCallback deliverCallback = (s, delivery) -> {
String message = new String(delivery.getBody(), "UTF-8");
System.out.println(" [x] Received '" + message + "'");
};
//等待消息
channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> {
        });
```

3.效果展示  
a.只启动Publish程序，会发现消息已经已经开始进入交换机，但是我们并没有定义queue去存这些消息，所以消息是不保存的
![img.png](img.png)
![img_1.png](img_1.png)
b.启动一个Subscribe 实例，也可以从打印看出，在之前的消息都被rabbit丢弃了
![img_4.png](img_4.png)
![img_3.png](img_3.png)
![img_2.png](img_2.png)
c.再启动一个Subscribe实例,观察效果与#b类似，都是从队列建立时获取消息，之前的消息都丢弃
![img_6.png](img_6.png)
![img_5.png](img_5.png)


###总结
1. Exchange的广播模式：com.rabbitmq.client.BuiltinExchangeType.FANOUT
2. 随机生成队列：队列名称由rabbit生成的一串随机字符串，且这个队列是non-durable，auto-delete，exclusive的
```java
String queueName = channel.queueDeclare().getQueue();
```

