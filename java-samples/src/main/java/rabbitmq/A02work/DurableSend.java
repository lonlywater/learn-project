package rabbitmq.A02work;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.MessageProperties;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeoutException;

/***
 * 以官方的例子改编，“.”一个点表示停顿1s
 * 以命令行键入的方式来发送消息
 *
 * 开启队列的持久化，发送消息以后，将rabbitmq重启，看消息是否会丢失
 *
 */
public class DurableSend {
    private static final String QUEUE_NAME = "noidler_queue2";

    public static void main(String[] args) throws IOException, TimeoutException {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost("192.168.1.100");
        connectionFactory.setUsername("guest");
        connectionFactory.setPassword("guest");
        //队列的持久化开关
        Boolean durableFlag = true;
        try (Connection connection = connectionFactory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_NAME, durableFlag, false, false, null);
            while (true) {
                String message = new BufferedReader(new InputStreamReader(System.in)).readLine();

                channel.basicPublish("", QUEUE_NAME, MessageProperties.PERSISTENT_TEXT_PLAIN, message.getBytes());
//                System.out.println(" [Noidler] Sent '" + message + "'");
            }

        }
    }
}
