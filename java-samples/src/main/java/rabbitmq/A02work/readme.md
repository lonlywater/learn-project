##work queue demo
此demo展示 rabbitmq官网示例中的第2种工作模式，work queue  
![](img_1.png)  
###特点
这种工作模式下，producer产生的消息:  
1.默认，轮序consumer，逐次分配，各个consumer总是分得大致相同个task
![](img.png)
2.如果worker1分到一个要执行很久的任务，且未完成，并且，后续已经分发了更多的消息给这个worker的话，如果worker挂了，消息将会全部丢失    
解决方法：  
message acknowledgments.  消息确认机制  

在rabbitmq.work.ManuelAckRec中，我们将finally 代码块中的ack注释掉，会发现，consumer得到了这些message，消费了，但是无法ack的情况下，这些消息，会重新分配给其它的consumer（以ack consumer 下线直接测试）
![](img_2.png)  
如上图框中的消息，被ManualAck处理过，但是没有ack，然后做下线处理，另外两个consumer立马会收到重新分发的消息

3.持久化  
a.需要在declare的时候，申明queue的durable=true，允许队列持久化，但是还需要配置message持久化，否则，服务器重启会如下图所示，消息丢失
![](img_3.png)
b.消息持久化  
如下图所示，添加了messageproperty设置为消息持久化
![](img_4.png)
最终执行service rabbitmq-server restart以后，消息依然存在
![img_6.png](img_6.png)

4.basicQos=1 + auto_ack=false，实现消息按消费者能力来分发，而不是默认的公平分发
参考：rabbitmq.A02work.QosRec  
设置basicQos=1，只能一个一个的分发消息过来，让consumer来处理，
auto_ack=false,则需要手动来进行ack，这样，当某个consumer拿到一个需要执行很久的任务，其它的worker很闲的时候，就可以执行更多的任务
![img_5.png](img_5.png)  




消息的接收以及发送的形式上与demo1（hello world）是一致的


###注意点
1.**durable参数用于申明队列是否是可持久化的**  
2.**queue不允许使用不同的参数进行redefine，即不允许先申明一个durable=true的queue1，再使用durable=false去申明queue1**  