#Exchange: direct demo
这个demo即是官网第4个demo，展示的是ExchangeType=direct 时，rabbitmq的工作状态
![img.png](img.png)
如图，我们申明一个交换器为direct类型，然后通过routingKey/bindingKey的匹配关系，向队列中分发消息  
  
###特点
1. 消息发送需要指明routingkey
2. 消息接受需要绑定queue,exchange,bindingkey
3. 发挥作用就是routingkey与bindingkey匹配上
4. 一个queue可以绑定多个key,一个key也可以绑定多个queue, 当一个key绑定了所有的queue时，就是work queue的工作状态
###工作流程：  
1. producer：  
   a. 申明一个exchange,type=direct  
     ```java
   channel.exchangeDeclare(exchangeDeclare, BuiltinExchangeType.DIRECT);
     ```
   b. 发布消息时，输入RoutingKey参数,如下代码中的msgType  
      ```java
   channel.basicPublish(exchangeDeclare, msgType, null, String.format(msgFmt, msgType, incr++).getBytes());
      ```
2. consumer：  
    a. 申明交换器  
    b. 申明队列  
    c. 绑定交换器、队列、bindingkey  
    d. 开始等待消息  
   ```java
   //申明交换器
    channel.exchangeDeclare(exchangeDeclare, BuiltinExchangeType.DIRECT);
    //申明随机队列
   String queueName=channel.queueDeclare().getQueue();
   //绑定：队列，交换器，bindingkey
    channel.queueBind(queueName,exchangeDeclare,messageType[0]);
    channel.queueBind(queueName,exchangeDeclare,messageType[1]);
    //消费消息
    channel.basicConsume(queueName, true, "", (consumerTag, message) -> {
        String msg= new String(message.getBody(),"UTF-8");
        System.out.println("[Console]:"+msg);
    }, consumerTag -> {

    });
    ```
   

###注意点
在写这个demo的过程中，由于producer是while(true)在发送消息，我consumer复制代码过去，不小心将basicConsume代码放在了while(true)里，导致consumer的个数无限制的增长，最终吃光了rabbitmq的内存，rabbitmq开始阻塞producer发布消息，最终整个demo进入一个假死状态
![img_1.png](img_1.png)