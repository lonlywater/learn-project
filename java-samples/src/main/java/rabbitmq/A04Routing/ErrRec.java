package rabbitmq.A04Routing;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import rabbitmq.RabbitMQUtil;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class ErrRec {
    private static String exchangeDeclare = "logs2";
    private static String[] messageType = {"ERR"};

    public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
        Connection connection = RabbitMQUtil.newConnection();
        Channel channel = RabbitMQUtil.newChannel(connection);
        channel.exchangeDeclare(exchangeDeclare, BuiltinExchangeType.DIRECT);
        String queueName=channel.queueDeclare().getQueue();
        channel.queueBind(queueName,exchangeDeclare,messageType[0]);

        channel.basicConsume(queueName, true, "", (consumerTag, message) -> {
            String msg= new String(message.getBody(),"UTF-8");
            System.out.println("[File]:"+msg);
        }, consumerTag -> {

        });
    }
}
