package rabbitmq.A04Routing;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import rabbitmq.RabbitMQUtil;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class LogPublish {
    private static String exchangeDeclare = "logs2";
    private static Random random = new Random();
    private static String[] messageType = {"INFO", "WARNING", "ERR"};

    public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
        Connection connection = RabbitMQUtil.newConnection();
        Channel channel = RabbitMQUtil.newChannel(connection);
        channel.exchangeDeclare(exchangeDeclare, BuiltinExchangeType.DIRECT);
        Integer incr = 1;
        while (true) {
            String msgType = getMessageType();
            String msgFmt = "[%s]:message-[%d]";
            channel.basicPublish(exchangeDeclare, msgType, null, String.format(msgFmt, msgType, incr++).getBytes());
            TimeUnit.MILLISECONDS.sleep(100);
        }
    }

    private static String getMessageType() {
        Integer rand = random.nextInt() % 10;
        if (rand.compareTo(5) <= 0) return messageType[0];
        if (rand.compareTo(8) <= 0) return messageType[1];
        return messageType[2];
    }
}
