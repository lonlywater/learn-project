package rabbitmq.A04Routing;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.DeliverCallback;
import com.rabbitmq.client.Delivery;
import rabbitmq.RabbitMQUtil;

import java.io.IOException;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class InfoWarningRec {
    private static String exchangeDeclare = "logs2";
    private static String[] messageType = {"INFO", "WARNING"};

    public static void main(String[] args) throws IOException, TimeoutException, InterruptedException {
        Connection connection = RabbitMQUtil.newConnection();
        Channel channel = RabbitMQUtil.newChannel(connection);
        channel.exchangeDeclare(exchangeDeclare, BuiltinExchangeType.DIRECT);
        String queueName=channel.queueDeclare().getQueue();
        channel.queueBind(queueName,exchangeDeclare,messageType[0]);
        channel.queueBind(queueName,exchangeDeclare,messageType[1]);

        channel.basicConsume(queueName, true, "", (consumerTag, message) -> {
            String msg= new String(message.getBody(),"UTF-8");
            System.out.println("[Console]:"+msg);
        }, consumerTag -> {

        });
    }

}
