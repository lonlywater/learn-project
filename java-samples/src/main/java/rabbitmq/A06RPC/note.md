###RPC demo
此demo即是官网第6个demo，展示的是rabbitmq RPC工作模式。
![](img.png)
工作流程如上图所示。  

###特点
1. 客户端：  
   a. 参数：replayTo，指定callbackQueue  
   b. 参数：correlationId，只建立一个callback queue的时候，使用次id管理request&response
   
2. 服务端监听rpc_queue，有消息就处理。  
   a. 响应发挥replayTo指定的queue中  