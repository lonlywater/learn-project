package rabbitmq.A08springboot;

import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * 简单队列消息消费者
 * @author wuwentao
 */
@Component
@Slf4j
public class SecondQueueConsumer {
    /**
     * 监听一个简单的队列，队列不存在时候会创建
     * @param content 消息
     */
    @RabbitListener(containerFactory = "secondRabbitListenerContainer",queuesToDeclare = @Queue(name = SimpleQueueProducer.AMQP_SIMPLE_QUEUE))
    public void consumerSimpleMessage(String content, Message message, Channel channel) throws IOException {
        // 通过Message对象解析消息
        String messageStr = new String(message.getBody());
        log.info("通过参数形式接收的消息:{}" ,content);
        //log.info("通过Message:{}" ,messageStr); // 可通过Meessage对象解析消息
        // channel.basicAck(message.getMessageProperties().getDeliveryTag(), false); // 手动确认消息消费成功
        // channel.basicNack(message.getMessageProperties().getDeliveryTag(), false, true); // 手动确认消息消费失败
    }
}