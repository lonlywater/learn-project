package rabbitmq.A08springboot;

import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ExchangeFanoutConsumer {
    /**
     * 创建交换机并且绑定队列（队列1）
     *
     * @param content 内容
     * @param channel 通道
     * @param message 消息
     * @throws IOException      ioexception
     * @throws TimeoutException 超时异常
     */
    @RabbitListener(bindings = @QueueBinding(
            exchange = @Exchange(value = ExchangeFanoutProducer.EXCHANGE_FANOUT, durable = "true", type = ExchangeTypes.FANOUT),
            value = @Queue(value = ExchangeFanoutProducer.EXCHANGE_FANOUT_QUEUE_1, durable = "true")
    ))
    @RabbitHandler
    public void exchangeFanoutQueue1(String content, Channel channel, Message message) {
        log.info("EXCHANGE_FANOUT_QUEUE_1队列接收到消息：{}",content);
    }

    /**
     * 创建交换机并且绑定队列（队列2）
     */
    @RabbitListener(bindings = @QueueBinding(
            exchange = @Exchange(value = ExchangeFanoutProducer.EXCHANGE_FANOUT, durable = "true", type = ExchangeTypes.FANOUT),
            value = @Queue(value = ExchangeFanoutProducer.EXCHANGE_FANOUT_QUEUE_2, durable = "true")
    ))
    @RabbitHandler
    public void exchangeFanoutQueue2(String content, Channel channel, Message message) {
        log.info("EXCHANGE_FANOUT_QUEUE_2队列接收到消息：{}",content);
    }

}