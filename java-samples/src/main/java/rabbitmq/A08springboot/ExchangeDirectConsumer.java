package rabbitmq.A08springboot;

import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 直连交换机队列消费者
 * @author wuwentao
 */
@Component
@Slf4j
public class ExchangeDirectConsumer {
    /**
     * 创建交换机并且绑定队列1（绑定routingkey1）
     *
     * @param content 内容
     * @param channel 通道
     * @param message 消息
     */
    @RabbitListener(bindings = @QueueBinding(
            exchange = @Exchange(value = ExchangeDirectProducer.EXCHANGE_DIRECT, durable = "true", type = ExchangeTypes.DIRECT),
            value = @Queue(value = ExchangeDirectProducer.EXCHANGE_DIRECT_QUEUE_1, durable = "true"),
            key = ExchangeDirectProducer.EXCHANGE_DIRECT_ROUTING_KEY_1
    ))
    @RabbitHandler
    public void exchangeDirectRoutingKey1(String content, Channel channel, Message message) {
        log.info("队列1 KEY1接收到消息：{}",content);
    }
    @RabbitListener(bindings = @QueueBinding(
            exchange = @Exchange(value = ExchangeDirectProducer.EXCHANGE_DIRECT, durable = "true", type = ExchangeTypes.DIRECT),
            value = @Queue(value = ExchangeDirectProducer.EXCHANGE_DIRECT_QUEUE_1, durable = "true"),
            key = ExchangeDirectProducer.EXCHANGE_DIRECT_ROUTING_KEY_1
    ),
    containerFactory = "secondRabbitListenerContainer")
    @RabbitHandler
    public void secondExchangeDirectRoutingKey1(String content, Channel channel, Message message) {
        log.info("second的队列1 KEY1接收到消息：{}",content);
    }

    /**
     * 创建交换机并且绑定队列2（绑定routingkey2）
     */
    @RabbitListener(bindings = @QueueBinding(
            exchange = @Exchange(value = ExchangeDirectProducer.EXCHANGE_DIRECT, durable = "true", type = ExchangeTypes.DIRECT),
            value = @Queue(value = ExchangeDirectProducer.EXCHANGE_DIRECT_QUEUE_2, durable = "true"),
            key = ExchangeDirectProducer.EXCHANGE_DIRECT_ROUTING_KEY_2
    ),containerFactory = "secondRabbitListenerContainer")
    @RabbitHandler
    public void exchangeDirectRoutingKey2(String content, Channel channel, Message message) {
        log.info("队列2 KEY2接收到消息：{}",content);
    }

}