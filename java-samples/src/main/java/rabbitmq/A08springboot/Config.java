package rabbitmq.A08springboot;

import org.json.JSONObject;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.core.ExchangeBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.QueueBuilder;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.amqp.RabbitProperties;
import org.springframework.boot.autoconfigure.amqp.SimpleRabbitListenerContainerFactoryConfigurer;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.PropertyMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import java.time.Duration;

/**
 * @Author: Administrator
 * @Date: 2023/2/17 10:53
 */

@Configuration
@ConfigurationProperties(prefix = "rabbitmq")
public class Config {



    //自定义rabbitmq.second
    RabbitProperties second;

    public RabbitProperties getSecond() {
        return second;
    }

    public void setSecond(RabbitProperties second) {
        this.second = second;
    }


    @Bean("rabbitTemplate")
    public RabbitTemplate rabbitTemplate(@Qualifier("connectionFactory")ConnectionFactory secondConnectionFactory){
        RabbitTemplate template=new RabbitTemplate(secondConnectionFactory);
//        template.setMessageConverter(new Jackson2JsonMessageConverter());

        return template;
    }
    @Bean("connectionFactory")
    public ConnectionFactory connectionFactory(RabbitProperties rabbitProperties){
        CachingConnectionFactory connectionFactory=new CachingConnectionFactory();

        System.out.println(rabbitProperties.getVirtualHost());
        connectionFactory.setHost(rabbitProperties.getHost());
        connectionFactory.setVirtualHost(rabbitProperties.getVirtualHost());
        connectionFactory.setUsername(rabbitProperties.getUsername());
        connectionFactory.setPassword(rabbitProperties.getPassword());
        connectionFactory.setPort(rabbitProperties.getPort());
        return connectionFactory;
    }
    @Bean("rabbitListenerContainer")
    public SimpleRabbitListenerContainerFactory rabbitListenerContainer( SimpleRabbitListenerContainerFactoryConfigurer configurer,
                                                                               @Qualifier("connectionFactory") ConnectionFactory connectionFactory) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        configurer.configure(factory, connectionFactory);
        return factory;
    }
    //这个对象控制力chanel的复用，binding queue exchange的所有自动申明 和创建
    @Bean("rabbitAdmin")
    public RabbitAdmin rabbit(@Qualifier("rabbitTemplate") RabbitTemplate template){
        RabbitAdmin admin=new RabbitAdmin(template);
        return admin;
    }



    @Bean("secondConnectionFactory")
    public ConnectionFactory testConnectionFactory(){
        CachingConnectionFactory connectionFactory=new CachingConnectionFactory();

        System.out.println(second.getVirtualHost());
        connectionFactory.setHost(second.getHost());
        connectionFactory.setVirtualHost(second.getVirtualHost());
        connectionFactory.setUsername(second.getUsername());
        connectionFactory.setPassword(second.getPassword());
        connectionFactory.setPort(second.getPort());
        connectionFactory.setChannelCacheSize(10);
        return connectionFactory;
//        PropertyMapper map = PropertyMapper.get();
////        CachingConnectionFactory factory = new CachingConnectionFactory(
////                getRabbitConnectionFactoryBean(properties).getObject());
//        map.from(second::determineAddresses).to(factory::setAddresses);
//        map.from(second::isPublisherReturns).to(factory::setPublisherReturns);
//        map.from(second::getPublisherConfirmType).whenNonNull().to(factory::setPublisherConfirmType);
//        RabbitProperties.Cache.Channel channel = second.getCache().getChannel();
//        map.from(channel::getSize).whenNonNull().to(factory::setChannelCacheSize);
//        map.from(channel::getCheckoutTimeout).whenNonNull().as(Duration::toMillis)
//                .to(factory::setChannelCheckoutTimeout);
//        RabbitProperties.Cache.Connection connection = second.getCache().getConnection();
//        map.from(connection::getMode).whenNonNull().to(factory::setCacheMode);
//        map.from(connection::getSize).whenNonNull().to(factory::setConnectionCacheSize);
//        return factory;
    }
    @Bean("secondTemplate")
    public RabbitTemplate secondTemplate(ConnectionFactory secondConnectionFactory){
        RabbitTemplate template=new RabbitTemplate(secondConnectionFactory);
//        template.setMessageConverter(new Jackson2JsonMessageConverter());

        return template;
    }
    //这个对象控制力chanel的复用
    @Bean("secondAdmin")
    public RabbitAdmin adm(@Qualifier("secondTemplate") RabbitTemplate template){
        RabbitAdmin admin=new RabbitAdmin(template);
        return admin;
    }

    @Bean("secondRabbitListenerContainer")
    public SimpleRabbitListenerContainerFactory secondRabbitListenerContainer( SimpleRabbitListenerContainerFactoryConfigurer configurer,
    @Qualifier("secondConnectionFactory") ConnectionFactory connectionFactory) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        configurer.configure(factory, connectionFactory);
        return factory;
    }

//    //交换机名称
//    public static final String ITEM_EXCHANGE = "item_exchange";
//    //队列名称
//    public static final String ITEM_QUEUE = "item_queue";

//    //声明交换机
//    @Bean("itemExchange")
//    public Exchange itemExchange(@Qualifier("secondAdmin")RabbitAdmin admin){
//        return ExchangeBuilder.directExchange(ITEM_EXCHANGE).admins(admin).durable(true).build();
//    }
//
//    //声明队列
//    @Bean("itemQueue")
//    public Queue itemQueue(){
//        return QueueBuilder.durable(ITEM_QUEUE).build();
//    }
//
//    //绑定队列和交换机
//    @Bean
//    public Binding itemQueueExchange(@Qualifier("itemQueue") Queue queue,
//                                     @Qualifier("itemExchange") Exchange exchange){
//        return BindingBuilder.bind(queue).to(exchange).with("info").noargs();
//    }



}

