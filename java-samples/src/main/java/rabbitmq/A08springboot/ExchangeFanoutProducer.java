package rabbitmq.A08springboot;

import lombok.AllArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/exchange/fanout")
public class ExchangeFanoutProducer {
    @Autowired
    private RabbitTemplate rabbitTemplate;
    // 扇形交换机定义
    public static final String EXCHANGE_FANOUT = "exchange.fanout";
    // 绑定扇形交换机的队列1
    public static final String EXCHANGE_FANOUT_QUEUE_1 = "exchange.fanout.queue1";
    // 绑定扇形交换机的队列2
    public static final String EXCHANGE_FANOUT_QUEUE_2 = "exchange.fanout.queue2";

    /**
     * 发送扇形消息消息能够被所有绑定该交换机的队列给消费
     *
     * @param message 消息内容
     */
    @GetMapping("/sendMessage")
    public String sendMessage(@RequestParam(value = "message") String message) {
        // routingkey 在fanout模式不使用，会在direct和topic模式使用,所以这里给空
        rabbitTemplate.convertAndSend(EXCHANGE_FANOUT, "", message);
        return "OK";
    }
}