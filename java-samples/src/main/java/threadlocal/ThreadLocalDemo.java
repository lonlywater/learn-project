package threadlocal;

/**
 * ThreadLocal
 *    set(T v)，通过Thread中的threadLocals变量，java.lang.ThreadLocal.ThreadLocalMap
 *    以Threadlocal对象为key，存放这个value
 */
public class ThreadLocalDemo {

    class MyThreadLocal<T> extends ThreadLocal<T>{


    }

    public static void main(String[] args) {

        ThreadLocal<Integer> threadLocal = new ThreadLocal<>();
        new Thread(()->{
            threadLocal.set(Integer.valueOf(9999));
            System.gc();
            System.out.println("x");
            Integer x=threadLocal.get();
            System.out.println(x);
        }).start();
        threadLocal.set(123);
    }
}
