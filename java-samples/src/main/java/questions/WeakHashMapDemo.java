package questions;

import java.util.HashMap;
import java.util.WeakHashMap;

public class WeakHashMapDemo {
    public static void main(String[] args) {
        hashMapDemo();
        System.out.println("=============");
        weakHashMap();
    }



    private static void hashMapDemo() {
        HashMap<Integer,String> map =new HashMap<>();
        Integer integer= new Integer("2");
        String val="xxf";
        map.put(integer,val);
        System.out.println(map);
    }
    private static void weakHashMap() {
        WeakHashMap<Integer, String> map = new WeakHashMap<>();
//        Integer integer = 2;  0~127 VM有缓存，直接用的话，即是发生GC也无法回收
        Integer integer = new Integer(2);
        String val = "xxf";
        map.put(integer, val);
        integer = null;
        System.gc();
        System.out.println(map);

    }
}
