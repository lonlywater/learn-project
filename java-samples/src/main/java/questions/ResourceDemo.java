package questions;

import org.apache.catalina.webresources.FileResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**@
 *
 * @see FileSystemResource  从文件系统中读取
 *
 * @see ClassPathResource classpath读取
 * @Author: Administrator
 * @Date: 2023/4/13 14:43
 */

public class ResourceDemo {


    public static void main(String[] args) throws IOException {

        FileSystemResource fileSystemResource= new FileSystemResource("d://a.txt");
        System.out.println(fileSystemResource.getURL());
        if (fileSystemResource.exists()) {
            byte[] nute=new byte[1024];
            fileSystemResource.getInputStream().read(nute);
            System.out.println(new String(nute, StandardCharsets.UTF_8));
        }


        ClassPathResource classPathResource =new ClassPathResource("a.txt");
        if (classPathResource.exists()) {
            byte[] nute=new byte[1024];
            classPathResource.getInputStream().read(nute);
            System.out.println(new String(nute));
        }
    }

}

