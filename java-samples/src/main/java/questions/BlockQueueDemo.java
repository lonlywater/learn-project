package questions;

import org.junit.jupiter.api.Test;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * 阻塞队列
 *    无元素时，获取元素Block
 *    满元素时，插入元素Block
 */
public class BlockQueueDemo {
    public static void main(String[] args) {

    }

    /**
     * add 超限制，抛异常
     * element 探测头元素
     * remove 超限制，抛异常
     * remove 带参数则不会
     */
    @Test
    public void test1(){
        BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<String>(3);
        /**add -->remove组，block 抛异常**/
        System.out.println(blockingQueue.add("xxx"));
        System.out.println(blockingQueue.add("xxx"));
        System.out.println(blockingQueue.add("xxx"));

        //队头元素
        System.out.println(blockingQueue.element());

        System.out.println(blockingQueue.remove());
        System.out.println(blockingQueue.remove());
        System.out.println(blockingQueue.remove());
        System.out.println(blockingQueue.remove("xxx"));
        System.out.println(blockingQueue.remove());
        /**add -->remove组，block 抛异常**/
    }
    /**
     * offer 超限制，不阻塞，返回false
     * peek 探测头元素
     * poll 超限制，不阻塞，返回null
     */
    @Test
    public void test3() {
        BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<String>(3);
        System.out.println(blockingQueue.offer("xxx"));
        System.out.println(blockingQueue.offer("a"));
        System.out.println(blockingQueue.offer("b"));
        System.out.println(blockingQueue.offer("c"));
        ;
        //队头元素
        System.out.println(blockingQueue.peek());


        System.out.println(blockingQueue.poll());
        System.out.println(blockingQueue.poll());
        System.out.println(blockingQueue.poll());
        System.out.println(blockingQueue.poll());
        System.out.println(blockingQueue.poll());
    }
    /**
     * put 超限制，阻塞
     * take 超限制，阻塞
     */
    @Test
    public void test2() throws InterruptedException {
        BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<String>(3);
        blockingQueue.put("xxx");
        blockingQueue.put("a");
        blockingQueue.put("b");
        ;
        //队头元素
        System.out.println(blockingQueue.take());
        System.out.println(blockingQueue.take());
        System.out.println(blockingQueue.take());
        System.out.println(blockingQueue.take());
    }

    /**
     * offer 超限制，不阻塞
     * peek 探测头元素
     * poll 超限制，不阻塞
     */
    @Test
    public void test4() throws InterruptedException {
        BlockingQueue<String> blockingQueue = new ArrayBlockingQueue<String>(3);
        System.out.println(blockingQueue.offer("xxx",2L, TimeUnit.SECONDS));
        System.out.println(blockingQueue.offer("A",2L, TimeUnit.SECONDS));
        System.out.println(blockingQueue.offer("B",2L, TimeUnit.SECONDS));
        System.out.println(blockingQueue.offer("C",2L, TimeUnit.SECONDS));
        //队头元素
        System.out.println(blockingQueue.peek());
        System.out.println("==============");
        System.out.println(blockingQueue.poll(2L, TimeUnit.SECONDS));
        System.out.println(blockingQueue.poll(2L, TimeUnit.SECONDS));
        System.out.println(blockingQueue.poll(2L, TimeUnit.SECONDS));
        System.out.println(blockingQueue.poll(2L, TimeUnit.SECONDS));
    }
}
