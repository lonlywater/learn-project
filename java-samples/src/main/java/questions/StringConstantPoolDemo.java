package questions;

import org.junit.jupiter.api.Test;

public class StringConstantPoolDemo {

    @Test
    public void case1() {
        String s1 = new String("do");
        s1.intern();
        String s2 = "do";
        System.out.println(s1 == s2);//false
        System.out.println(s1.intern() == s2);//true

        String s3 = new String("12") + new String("34");
//        s3.intern();//有沒有這一行代碼，結果可能不同
        String s4 = "1234";//始终等于常量池地址
        System.out.println(s3 == s4);
        System.out.println(s3.intern() == s4);
        System.out.println(s3.intern() == s3);

    }

    public static void main(String[] args) {
        new StringConstantPoolDemo().case1();
//        String xx = "1.8.0_152";
//        String s = new StringBuilder("1.8.0_").append("152").toString();
//        System.out.println(s == s.intern());
//        System.out.println(s == xx);
//        System.out.println(xx == s.intern());
//        System.out.println("------------------------");
//
//        String xx2 = "58tongcheng";
//        String xx4 = "58tongcheng";
//        String xx3 = new String("58tongcheng");
//        String s2 = new StringBuilder("58").append("tongcheng").toString();
//        System.out.println(xx2);
//        System.out.println(s2);
//        System.out.println(s2 == s2.intern());
//        System.out.println(xx2 == s2.intern());
//        System.out.println(xx2 == s2);
//        System.out.println(xx2 == xx3);
//        System.out.println(xx2 == xx4);

    }


}
