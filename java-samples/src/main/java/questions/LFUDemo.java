package questions;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class LFUDemo {
    public static void main(String[] args) {
//        LFUCache<Integer, Integer> cache = new LFUCache<>(2);
//        cache.put(1, 1);
//        cache.put(2, 2);
//        cache.get(1);
//        cache.put(3, 3);
//        cache.get(2);
//        cache.get(3);
//        cache.put(4, 4);
//        cache.get(1);
//        cache.get(3);
//        cache.get(4);

        LFUCache cache = new LFUCache(0);
        cache.put(0,0);
        cache.get(0);
        System.out.println(cache);
    }
}

/***
 * 最不常使用，即需要定义每一个key的访问次数，
 * 约定：
 *    队头为访问次数最高的节点
 *    队尾为访问次数最少的节点
 *    新插入的 平局时，放在所有平局节点的前面
 */
class LFUCache {
    int minfreq, capacity;
    Map<Integer, Node> nodeMap;
    Map<Integer, LinkedList<Node>> freqTable;

    public LFUCache(int capacity) {
        this.minfreq = 0;
        this.capacity = capacity;
        nodeMap = new HashMap<Integer, Node>();;
        freqTable = new HashMap<Integer, LinkedList<Node>>();
    }

    public int get(int key) {
        if (capacity == 0) {
            return -1;
        }
        if (!nodeMap.containsKey(key)) {
            return -1;
        }
        Node node = nodeMap.get(key);
        int val = node.val, freq = node.freq;
        freqTable.get(freq).remove(node);
        // 如果当前链表为空，我们需要在哈希表中删除，且更新minFreq
        if (freqTable.get(freq).size() == 0) {
            freqTable.remove(freq);
            if (minfreq == freq) {
                minfreq += 1;
            }
        }
        // 插入到 freq + 1 中
        LinkedList<Node> list = freqTable.getOrDefault(freq + 1, new LinkedList<Node>());
        list.offerFirst(new Node(key, val, freq + 1));
        freqTable.put(freq + 1, list);
        nodeMap.put(key, freqTable.get(freq + 1).peekFirst());
        return val;
    }

    public void put(int key, int value) {
        if (capacity == 0) {
            return;
        }
        if (!nodeMap.containsKey(key)) {
            // 缓存已满，需要进行删除操作
            if (nodeMap.size() == capacity) {
                // 通过 minFreq 拿到 freq_table[minFreq] 链表的末尾节点
                Node node = freqTable.get(minfreq).peekLast();
                nodeMap.remove(node.key);
                freqTable.get(minfreq).pollLast();
                if (freqTable.get(minfreq).size() == 0) {
                    freqTable.remove(minfreq);
                }
            }
            LinkedList<Node> list = freqTable.getOrDefault(1, new LinkedList<Node>());
            list.offerFirst(new Node(key, value, 1));
            freqTable.put(1, list);
            nodeMap.put(key, freqTable.get(1).peekFirst());
            minfreq = 1;
        } else {
            // 与 get 操作基本一致，除了需要更新缓存的值
            Node node = nodeMap.get(key);
            int freq = node.freq;
            freqTable.get(freq).remove(node);
            if (freqTable.get(freq).size() == 0) {
                freqTable.remove(freq);
                if (minfreq == freq) {
                    minfreq += 1;
                }
            }
            LinkedList<Node> list = freqTable.getOrDefault(freq + 1, new LinkedList<Node>());
            list.offerFirst(new Node(key, value, freq + 1));
            freqTable.put(freq + 1, list);
            nodeMap.put(key, freqTable.get(freq + 1).peekFirst());
        }
    }
}

class Node {
    int key, val, freq;

    Node(int key, int val, int freq) {
        this.key = key;
        this.val = val;
        this.freq = freq;
    }
}