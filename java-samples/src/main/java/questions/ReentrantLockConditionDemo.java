package questions;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 使用ReentrantLock实现精确唤醒符合条件的线程
 * <p>
 * 例子：
 * A线程打印5次-->B线程打印10次-->C线程打印15次-->A线程
 * 往复执行10次
 */
public class ReentrantLockConditionDemo {
    public static void main(String[] args) {
        /**使用ReentrantLock的例子*/
//        testReentrantLockSignalActualThread();
//
        testSynchronizedActualThreadnotifyAll();
    }

    private static void testReentrantLockSignalActualThread() {
        ShareResource shareResource = new ShareResource();
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                shareResource.print5();
            }, "AA").start();
            new Thread(() -> {
                shareResource.print10();
            }, "BB").start();
            new Thread(() -> {
                shareResource.print15();
            }, "CC").start();
        }
    }

    private static void testSynchronizedActualThreadnotifyAll() {
        SychShareResource shareResource = new SychShareResource();
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                shareResource.print5();
            }, "AA").start();
            new Thread(() -> {
                shareResource.print10();
            }, "BB").start();
            new Thread(() -> {
                shareResource.print15();
            }, "CC").start();
        }
    }
}

/**
 * 多线程操作资源，资源应该做到高内聚
 */
class ShareResource {
    private int number = 1;
    private Lock lock = new ReentrantLock();
    private Condition condition1 = lock.newCondition();
    private Condition condition2 = lock.newCondition();
    private Condition condition3 = lock.newCondition();

    public void print5() {
        lock.lock();
        while (number != 1) {
            try {
                condition1.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        for (int i = 0; i < 5; i++) {
            System.out.println(Thread.currentThread().getName() + "\t"+(1+i));
        }
        number = 2;
        condition2.signal();
        lock.unlock();

    }

    public void print10() {
        lock.lock();
        while (number != 2) {
            try {
                condition2.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        for (int i = 0; i < 10; i++) {
            System.out.println(Thread.currentThread().getName() + "\t"+(1+i));
        }
        number = 3;
        condition3.signal();
        lock.unlock();
    }

    public void print15() {
        lock.lock();
        while (number != 3) {
            try {
                condition3.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        for (int i = 0; i < 15; i++) {
            System.out.println(Thread.currentThread().getName() + "\t"+(1+i));
        }
        number = 1;
        condition1.signal();
        lock.unlock();
    }
}
/**
 * 多线程操作资源，使用synchronized关键字实现例子，作为对比
 * notify     -- 唤醒单个正在等待当前锁对象的线程，在本案例中，可能导致死锁，A、C在等，B执行完，唤醒A, A继续等，C继续等，就不会再执行
 * notufyAll  -- 唤醒等待当前锁对象的线程，总能保证该执行的线程被唤醒
 */
class SychShareResource {
    private int number = 1;

    public synchronized void print5() {
        while (number != 1) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        for (int i = 0; i < 5; i++) {
            System.out.println(Thread.currentThread().getName() + "\t"+(1+i));
        }
        number = 2;
        this.notifyAll();
    }

    public synchronized void print10() {
        while (number != 2) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        for (int i = 0; i < 10; i++) {
            System.out.println(Thread.currentThread().getName() + "\t"+(1+i));
        }
        number = 3;
        this.notifyAll();
    }

    public synchronized void print15() {
        while (number != 3) {
            try {
                this.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        for (int i = 0; i < 15; i++) {
            System.out.println(Thread.currentThread().getName() + "\t"+(1+i));
        }
        number = 1;
        this.notifyAll();
    }
}