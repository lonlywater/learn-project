package questions;

import java.lang.ref.PhantomReference;
import java.lang.ref.ReferenceQueue;

public class PhantomReferenceDemo {
    public static void main(String[] args) {
        Object o = new Object();
        ReferenceQueue<Object> objectReferenceQueue = new ReferenceQueue<>();
        PhantomReference<Object> objectPhantomReference= new PhantomReference<>(o,objectReferenceQueue);
        System.out.println(o);
        System.out.println(objectPhantomReference);
        System.out.println(objectReferenceQueue.poll());
        o= null;
        System.gc();
        System.out.println("-------------");
        System.out.println(o);
        System.out.println(objectPhantomReference);
        System.out.println(objectReferenceQueue.poll());
    }
}
