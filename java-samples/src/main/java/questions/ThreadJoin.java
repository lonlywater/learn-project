package questions;

import lombok.SneakyThrows;

/**
 * 使用join方法实现线程顺序执行
 * join()=join(0)，等待被调用线程死亡
 * join(1000)，等待被调用线程死亡,最多等一秒，没结束也执行
 */
public class ThreadJoin {
    public static void main(String[] args) {
        Thread thread = new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                Thread.sleep(10000);
                System.out.println("线程："+Thread.currentThread().getName());
            }
        },"A");
        Thread thread2 = new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                thread.join(2000);
//                thread.join(0);
                System.out.println("线程："+Thread.currentThread().getName());
            }
        },"B");
        Thread thread3 = new Thread(new Runnable() {
            @SneakyThrows
            @Override
            public void run() {
                thread2.join();
                System.out.println("线程："+Thread.currentThread().getName());
            }
        },"C");
        System.out.println("开始");
        thread.start();;
        thread2.start();;
        thread3.start();
    }
}
