package questions;

/**
 * 用于测试基本数据类型的字节长度
 */
public class BaseTypeByteLenth {
    public static void main(String[] args) {
        //Character   utf-16
        System.out.println("Character "+Character.BYTES);
        //Integer
        System.out.println("Integer"+Integer.BYTES);
        System.out.println("Byte "+Byte.BYTES);
        System.out.println("Short "+Short.BYTES);
        System.out.println("Long  "+Long.BYTES);
        System.out.println("Double"+Double.BYTES);
        System.out.println("Float "+Float.BYTES);

    }
}
