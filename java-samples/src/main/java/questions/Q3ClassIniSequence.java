package questions;

import java.security.AccessControlException;

/**
 *1.
 */
public class Q3ClassIniSequence {
    public static void main(String[] args) {
        Son s1 = new Son();
        System.out.println();
        Son s2 = new Son(1);
    }
}

/**
 * 1.加载parent
 *   1.执行<clinit>静态代码块+静态属性赋值语句，按出现顺序执行，且只执行一次
 * 2.初始化实例，执行<init>
 *     #非静态代码块+非静态属性显示赋值语句，谁先出现执行谁
 *     #最后执行构造方法中的语句
 *     #这里的赋值语句如果是方法，考虑是否有被子类重写，只有实例且非private方法，会被重写
 */
class Parent {
    private static int q = method();
    static {
        System.out.print("（1）");
    }

    private static int j = method();
    private int m = test();
    Parent() {
        System.out.print("（2）");
    }
    private int n = test();
    {
        System.out.print("（3）");
    }

    private int i = test();

    public int test() {
        System.out.print("（4）");
        return 0;
    }

    public static int method() {
        System.out.print("（5）");
        return 0;
    }
}

/**
 * 1，要加载Son,先加载Parent
 * 2. Son类的静态代码块+静态属性赋值语句按出现顺序执行，且只执行一次
 * 3. 初始化实例，先super(),再执行<init>
 *       #非静态代码块+非静态属性显示赋值语句，谁先出现执行谁
 *       #最后执行构造方法中的语句
 *       #这里的赋值语句如果是方法，考虑是否有被子类重写，只有实例且非private方法，会被重写
 */
class Son extends Parent {
    private int i = test();
    private static int j = method();

    static {
        System.out.print("（6）");
    }
    Son(int x){
        System.out.print("（11）");
    }

    Son() {
        System.out.print("（7）");
    }

    {
        System.out.print("（8）");
    }


    public int test() throws RuntimeException {
        System.out.print("（9）");
        return 0;
    }


    public static int method() {
        System.out.print("（10）");
        return 0;
    }
}
