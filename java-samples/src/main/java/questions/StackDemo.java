package questions;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class StackDemo {
    public static void main(String[] args) {
        test2();
    }

    /**
     * @see Deque deque接口中带有stack操作方法
     * @see LinkedList 作为双向链表，实现了Deque的接口 ， fail-fast，不支持多线程环境下使用
     */
    private static void test2() {
        Deque<String> stringLinkedList = new LinkedList<>();
        stringLinkedList.push("xxf");
        System.out.println(stringLinkedList.pop());
        System.out.println(stringLinkedList.peek());
    }
    /**
     *
     */

    /**
     * @see Stack 类，基于Vector，线程安全，利用synchronize关键字实现
     * peek()空栈时，会抛出异常
     */
    public static void test1(){
        Stack stack1= new Stack();
        stack1.push("xxf");
        System.out.println(stack1.peek());
        System.out.println(stack1.pop());
        System.out.println(stack1.peek());
    }
}
