package questions;

/**
 * case1:正常执行
 *
 * case2:-XX:-DoEscapeAnalysis -XX:-EliminateLocks执行
 */
public class EscapeAnalysAndElimitLock {
    public static void main(String[] args) {
        long curr= System.currentTimeMillis();
        String s="";
        for (int i = 0; i < 10000; i++) {
            s= getString("test",s+"+"+i);
        }
        System.out.println(System.currentTimeMillis()-curr);
    }
    public static String getString(String a, String b){
        StringBuffer stringBuffer  = new StringBuffer();
        stringBuffer.append(a).append(b);
        return stringBuffer.toString();
    }
}
