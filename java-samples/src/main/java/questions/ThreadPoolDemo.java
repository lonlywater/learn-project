package questions;

import org.junit.jupiter.api.Test;

import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/***
 * 线程池支持两种
 *   1.带返回值
 *   2.不带返回值
 */
public class ThreadPoolDemo {
    @Test
    public void testThread(){
        Thread thread =new Thread();
//        thread.wait();
//        thread.notify();
        thread.start();
        thread.run();
    }
    @Test
    public void testExecutors(){
        Executors.newScheduledThreadPool(3);
        Executors.newCachedThreadPool();
        Executors.newSingleThreadExecutor();
        Executors.newFixedThreadPool(10);
        ExecutorService executor=Executors.newWorkStealingPool(5);
        Thread thread =new Thread();
        thread.suspend();
    }
    @Test
    public void testhreadPool(){
        //一池固定5个线程
        ExecutorService executor=new ThreadPoolExecutor(5,6,1000,TimeUnit.MILLISECONDS,new LinkedBlockingQueue<>(3)
                , Executors.defaultThreadFactory());
        //一池1线程
//        ExecutorService executor=Executors.newSingleThreadExecutor();
        //一池N线程
//        ExecutorService executor=Executors.newCachedThreadPool();
//        ExecutorService executor=Executors.newWorkStealingPool();
        //模拟10个用户办业务
        try {
            for (int i = 0; i < 10; i++) {
                final int intI=i+1;
                executor.execute(()->{
                    System.out.println(Thread.currentThread().getName()+"\t"+"办理业务"+intI);
                });
//                TimeUnit.MILLISECONDS.sleep(100);
            }
        }
//        catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        finally {
            executor.shutdown();
        }

    }



    /**
     * 同一个future task在不同线程里也只会执行一次
     */
    @Test
    public void testRuntime(){
        System.out.println(Runtime.getRuntime().availableProcessors());
    }

    /**
     * 同一个future task在不同线程里也只会执行一次
     */
    @Test
    public void testCallableDemo(){
        FutureTask<Integer> futureTask = new FutureTask<>(new myThread());
        new Thread(futureTask,"test1").start();
        new Thread(futureTask,"test2").start();
        new Thread(futureTask,"tes3").start();
        System.out.println("main 线程执行");
        int result=100;
        while (!futureTask.isDone()){
            try {
                result+=futureTask.get();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        }

        System.out.println("执行结束，返回结果为："+result);
    }

}
class  myThread implements Callable<Integer>{
    @Override
    public Integer call() throws Exception {
        System.out.println(Thread.currentThread().getName()+"\t"+"callable 执行开始");
        TimeUnit.SECONDS.sleep(3);
        System.out.println(Thread.currentThread().getName()+"\t"+"callable 执行结束");
        return 100;
    }
}
