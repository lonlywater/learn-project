package questions;

import java.util.concurrent.SynchronousQueue;

public class SynchronousDemo {
    public static void main(String[] args) {
        SynchronousQueue<String> stringSynchronousQueue = new SynchronousQueue<>();
        new Thread(()->{
            try {
                stringSynchronousQueue.put("1");
                System.out.println("1");
                stringSynchronousQueue.put("2");
                System.out.println("2");
                stringSynchronousQueue.put("3");
                System.out.println("3");
                stringSynchronousQueue.put("4");
                System.out.println("4");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }).start();
        new Thread(()->{
            try {
                int i=5;
               while (i>0){
                   i--;
                   Thread.sleep(5000);
                   System.out.println(stringSynchronousQueue.take());
               }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }).start();
    }
}
