package questions;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;

public class CyclicBarrierDemo {
    public static void main(String[] args) throws InterruptedException {
        CyclicBarrier cyclicBarrier = new CyclicBarrier(7, () -> {
            //可以看到执行这个command是最后一个线程，执行完command会唤醒其它等待的线程
            System.out.println(Thread.currentThread().getName()+":召唤神龙出世");
        });
        for (int i = 0; i < 7; i++) {
            final int temp = i + 1;
            new Thread(() -> {
                System.out.println("龙珠" + temp + "到手");
                try {
                    //当前线程等待，并没有退出，当等的人数到期，先执行定于好的task，各个线程再继续做事儿
                    cyclicBarrier.await();
                    System.out.println("召唤结束，龙珠" + temp + "溜溜球");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }
            }, "" + i).start();
        }
    }
}
