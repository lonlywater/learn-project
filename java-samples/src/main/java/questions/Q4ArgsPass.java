package questions;

import java.util.Arrays;

/**
 * 知识点总结：
 * 1.String和封装类型的不可变性，一旦发生变化，则会产生新的对象
 * 2.参数传递：
 *     形参是基本类型，传值
 *     形参是封装类型，传应用【地址】
 */
public class Q4ArgsPass {
    volatile int x;
    public static void main(String[] args) {
        int i = 1;
        String str = "hello";
        Integer num = 200;
        int[] arr = {1, 2, 3, 4, 5};
        MyData my = new MyData();
        change(i, str, num, arr, my);
        System.out.println("i = " + i);
        System.out.println("Str = " + str);
        System.out.println("num = " + num);
        System.out.println("arr = " + Arrays.toString(arr));
        System.out.println("my.a = " + my.a);

    }
    public static void change(int j, String s, Integer n, int[] arr, MyData m) {
        j += 1;
        s += "world";
        n += 1;
        arr[0]++;
        m.a++;
    }
}
class MyData {
    int a = 10;
}