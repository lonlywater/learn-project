package thread;

/**
 * 主线程结束 进程会结束吗
 * 不一定
 * 1.会等子线程运行结束
 * 2.thread.setDaemon(true); 后 进程不会等待 会直接退出
 */
public class MainThread {
    public static void main(String[] args) {

        Thread thread = new Thread(() -> {
            try {
                Thread.sleep(15000l);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("-sub end-");
        });
//        thread.setDaemon(true);
        thread.start();
        System.out.println("main end");

    }
}
