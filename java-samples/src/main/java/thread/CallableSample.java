package thread;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.security.SecureRandom;
import java.util.Random;
import java.util.concurrent.Callable;

@Slf4j
@Data
public class CallableSample implements Callable {
    private Integer threadId;
    private ThreadLocal threadlocal;

    public CallableSample(Integer threadId, ThreadLocal local) {
        this.threadId = threadId;
        this.threadlocal=local;
    }

    @Override
    public Object call() throws Exception {
        Random random = new SecureRandom();
        // To simulate a heavy computation,
        // we delay the thread for some random time
        Integer randomNumber = random.nextInt(10);
        log.info("stop:{} seconds", randomNumber);
        Thread.sleep(randomNumber * 1000);
        if (threadlocal!=null){
            threadlocal.set("current thread:"+threadId+" current: val:"+randomNumber);
        }
        return randomNumber;
    }
}
