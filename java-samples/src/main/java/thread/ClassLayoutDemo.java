package thread;

import org.openjdk.jol.info.ClassLayout;

/**
 * @Author: Administrator
 * @Date: 2023/2/8 18:31
 * -XX:BiasedLockingStartupDelay=0  偏向锁延迟为0
 * -XX:+UseBiasedLocking  开启偏向锁
 */

public class ClassLayoutDemo {


    public static void main(String[] args) {
        Object lock = new Object();
        ClassLayout layout=ClassLayout.parseInstance(lock);

        System.out.println(layout.toPrintable());
        synchronized (lock){
            System.out.println(ClassLayout.parseInstance(lock).toPrintable());
        }

    }

}

