package thread;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

/**
 * @Author: Administrator
 * @Date: 2023/2/14 17:08
 */

public class CyclicBirrerExample {

    static CyclicBarrier cyclicBarrier=new CyclicBarrier(5,()->{
        System.out.println("执行结束了");
    });

    public static void main(String[] args) {
        for (int i = 0; i < 5; i++) {
            new Thread(()->{
                try {
                    Thread.sleep(2000);
                    cyclicBarrier.await();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                }


            }).start();



        }
    }

}

