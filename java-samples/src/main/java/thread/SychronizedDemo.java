package thread;

public class SychronizedDemo {
    private static Integer i = 0;

    public static void main(String[] args) throws InterruptedException {
        new Thread(() -> {
//            synchronizedMethod();
            synchronizedBlock();
        }, "A").start();

        new Thread(() -> {
            synchronizedBlock();
//            synchronizedMethod();
        }, "B").start();
        new Thread(() -> {
            synchronizedBlock2();
//            synchronizedMethod();
        }, "C").start();
        Thread.sleep(2000);
        System.out.println(i);
    }

    public static synchronized void synchronizedMethod() {
        for (int j = 0; j < 1000; j++) {
            i++;
        }
    }

    public static void synchronizedBlock() {
        synchronized (i) {
            for (int j = 0; j < 1000; j++) {
                i++;
            }
        }
    }

    public static void synchronizedBlock2() {

        for (int j = 0; j < 1000; j++) {
            synchronized (i) {
                i++;
            }
        }
    }
}
