package thread;

class InterruptD implements Runnable{

        @Override
        public void run() {
            
            
            while (!Thread.currentThread().isInterrupted()){
                try {
                    Thread.sleep(2000);

                } catch (InterruptedException e) {
                    System.out.println(Thread.currentThread().getName()+"子线程中断异常:"+Thread.currentThread().isInterrupted());
                    Thread.currentThread().interrupt();
                    System.out.println(Thread.currentThread().getName()+"接收到了中断异常信号，线程自身确认中断");
                }
            }

        }
    }