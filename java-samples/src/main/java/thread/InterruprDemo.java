package thread;

/**
 * @Author: Administrator
 * @Date: 2023/2/8 16:27
 *
 * interrupt只是给一个是否中断的信号。
 * 是否中断由线程自己决定
 *
 *
 */

public class InterruprDemo {
    public static void main(String[] args) throws InterruptedException {
        Thread thread =new Thread(new InterruptD(),"中断复位A");
        thread.start();
        Thread thread1=new Thread(new InterruptD2(),"正常中断B");
        thread1.start();

        Thread.sleep(2000);
        thread.interrupt();
        thread1.interrupt();


    }


}

