package spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import spring.User;

@Configuration
@ComponentScan(value = "spring.circle")
public class ConfigCircle1 {

    @Bean
    public User user(){
        return new User();
    }
}
