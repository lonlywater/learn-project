package spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import spring.bean.Person;

@Configuration
@ComponentScan({"spring.cyclicRef"})
@EnableAspectJAutoProxy
public class ConfigOfCyclicRef {

}
