package spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import spring.component.BookDao;

@ComponentScan("spring.component")
public class ConfigOfAutowared {

    @Bean("dao2")
    public BookDao dao2(){
        BookDao bookDao= new BookDao();
        bookDao.setLabel(2);
        return bookDao;
    }
}
