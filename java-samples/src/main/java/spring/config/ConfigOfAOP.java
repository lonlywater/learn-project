package spring.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import spring.bean.Person;

import javax.annotation.PostConstruct;

@Configuration
@ComponentScan({"spring.bean","spring.aop"})
@EnableAspectJAutoProxy
public class ConfigOfAOP {
    @Autowired Person person;
    @PostConstruct
    public void init(){
        System.out.println(person);
    }

    @Bean
    public Person person(){
        return new Person();
    }
}
