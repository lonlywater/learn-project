package spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import spring.bean.Person;

@Configuration
@PropertySource("classpath:/person.prop")
public class ConfigOfPropertySourcce {

    @Bean
    public Person person(){
        return new Person();
    }
}
