package spring.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class BookDao {
    private  Integer label=1;

    public Integer getLabel() {
        return label;
    }

    public void setLabel(Integer label) {
        this.label = label;
    }

    @Autowired
    JdbcTemplate template;

    public void insert(){
        String sql = "INSERT INTO `test`.`table1` (`m_name`, `addr`, `age`, `entry_id`, `entry_datetime`) " +
                "VALUES ('12', '11', '1', '23', '2021-01-27 21:39:24');";
        template.update(sql);
        System.out.println("插入成功！");
    }
}
