package spring.importdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class Main {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
        User bean = context.getBean(User.class);
        System.out.println(bean);
        context.close();
    }
}
