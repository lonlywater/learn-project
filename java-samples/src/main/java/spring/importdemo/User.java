package spring.importdemo;

import lombok.Data;

@Data
public class User {
    String name;
}
