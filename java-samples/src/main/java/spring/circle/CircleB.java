package spring.circle;

import org.springframework.stereotype.Component;

@Component
public class CircleB {
    CircleA a;

    public CircleB(CircleA b) {
        this.a = b;
    }
}
