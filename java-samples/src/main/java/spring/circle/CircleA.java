package spring.circle;

import org.springframework.stereotype.Component;

@Component
public class CircleA {
    CircleB b;

    public CircleA(CircleB b) {
        this.b = b;
    }
}
