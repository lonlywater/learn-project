package spring.cyclicRef;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AAspect2 {
    @Pointcut("execution(public void spring.cyclicRef.B.t*(..))")
    private void pointCut(){
    }

    @Before("pointCut()")
    public void before(){
        System.out.println("前置");

    }
}
