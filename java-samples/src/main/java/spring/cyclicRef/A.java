package spring.cyclicRef;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Setter
@Getter
public class A {
    @Autowired public B b;
    @Autowired public A a;
    public void test(){
        System.out.println("A test执行：");
        System.out.println(a);
        System.out.println(b);
    }
}
