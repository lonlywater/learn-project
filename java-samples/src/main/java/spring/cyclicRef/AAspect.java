package spring.cyclicRef;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AAspect {
    @Pointcut("execution(public void spring.cyclicRef.A.t*(..))")
    private void pointCut(){
    }

    @Before("pointCut()")
    public void before(JoinPoint joinPoint){
        System.out.println("前置："+joinPoint.toLongString());

    }
}
