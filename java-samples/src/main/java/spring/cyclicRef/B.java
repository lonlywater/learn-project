package spring.cyclicRef;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
@Getter
@Setter
public class B {

    @Resource
    A a;
    @Resource B b;

    public void test(){
        System.out.println("B test执行：");
        System.out.println(a);
        System.out.println(b);

    }
}
