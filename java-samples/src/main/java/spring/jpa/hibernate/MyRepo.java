package spring.jpa.hibernate;

import org.springframework.data.jpa.repository.JpaRepository;
import spring.jpa.bean.User;

public interface MyRepo extends JpaRepository<User,Integer> {
}
