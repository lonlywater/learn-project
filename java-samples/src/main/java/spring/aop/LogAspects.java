package spring.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Objects;

@Aspect
@Component
/**
 * @After在spring5版本中是在@AfterReturning之后执行
 * @After在spring4版本中是在@AfterReturning之前执行
 */
public class LogAspects {

    /**
     * 切点
     */
    @Pointcut("execution(public int spring.bean.MathCalculate.*(..))")
    public void pointCut(){

    }

    /**
     * 前置通知
     */
    @Before("pointCut()")
    public void  start(){
        System.out.println("Before除法");
    }

    /**
     * 后置通知
     */
    @After("pointCut()")
    public void  end(){
        System.out.println("After除法");
        System.out.println("After除法嗯？？？？？？？？？？？？？");
    }
    @Around("pointCut()")
    public Object  surround(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("surround start。。。");
        Object object = joinPoint.proceed(joinPoint.getArgs());
        System.out.println("surround end。。。");
        return object;
    }

    /**
     * 返回通知
     */
    @AfterReturning(value = "pointCut()",returning = "result")
    public void  returned(JoinPoint joinPoint, Object result){
        System.out.println("args:"+ Arrays.toString(joinPoint.getArgs()));
        System.out.println(String.format("AfterReturning除法" + result));
    }



    /**
     * 异常通知
     */
    @AfterThrowing(value = "pointCut()",throwing = "exception")
    public void  excep(Exception exception){
        System.out.println(String.format("除法异常。。。%s",exception.toString()));
    }
}
