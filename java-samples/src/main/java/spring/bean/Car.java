package spring.bean;

import javax.annotation.PostConstruct;

public class Car {
    public void init(){
        System.out.println("init car");
    }
    @PostConstruct
    public void postConstruct(){
        System.out.println("PostConstruct car");
    }
    public void destroy(){
        System.out.println("destroy");
    }
}
