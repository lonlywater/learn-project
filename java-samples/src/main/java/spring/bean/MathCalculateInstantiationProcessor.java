package spring.bean;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.InstantiationAwareBeanPostProcessor;
import org.springframework.stereotype.Component;

/**
 * 在实例化以前返回一个bean，并且这个bean需要AOP，看AOP是如何处理的
 */
@Component
public class MathCalculateInstantiationProcessor implements InstantiationAwareBeanPostProcessor {
    @Override
    public Object postProcessBeforeInstantiation(Class<?> beanClass, String beanName) throws BeansException {
        if (beanClass.getName().contains("MathCalculate")){
            System.out.println("手动在实例化前返回一个MathCalculate对象");
            //这里范湖的对象，相当于是完成了初始化的，后续会直接调用一初始化后后置处理器
            return new MathCalculate();
        }
        return null;
    }
}
