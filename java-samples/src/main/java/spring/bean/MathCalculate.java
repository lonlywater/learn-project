package spring.bean;


import org.springframework.stereotype.Component;

@Component
public class MathCalculate {
    public int div(Integer x, Integer y){
        return x/y;
    }
}
