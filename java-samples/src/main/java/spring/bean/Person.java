package spring.bean;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Data
@Component
public class Person {
    @Value(("#{20-2}"))
    Integer age;
    @Value("张三")
    String name;

    @Value("${nickname}")
    String nickname;
    @Autowired
    Car2 car2;
}
