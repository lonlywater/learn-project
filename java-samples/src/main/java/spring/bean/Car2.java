package spring.bean;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class Car2 implements InitializingBean, DisposableBean {

    @Autowired
    Person person;

    @Override
    public void destroy() throws Exception {
        System.out.println("destroyBy DisposableBean");
    }

    @PostConstruct
    public void init2(){
        System.out.println("PostConstruct");
    }
    @PreDestroy
    public void destroy2(){
        System.out.println("PreDestroy");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("init by afterPropertiesSet");
    }
}
