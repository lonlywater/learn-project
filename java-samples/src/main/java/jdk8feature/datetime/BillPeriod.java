package jdk8feature.datetime;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.time.LocalDate;

@Data
@ToString
public class BillPeriod implements Serializable {
    private LocalDate startDate;
    private LocalDate endDate;
    private Integer billDays;
    private Integer terms;
}
