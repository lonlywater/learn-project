package jdk8feature.stream;

import java.util.ArrayList;
import java.util.List;

public class SuperExtend {
    public static void main(String[] args) {

        List<? super P> list= new ArrayList<>();
        list.add(new P());
        list.add(new S());
        list.add(new T());


//        List<? extends T> list1= new ArrayList<>();
//        list1.add(new S());

    }
}

class P {}
class T extends P{
    String name;
}
class S extends T{}
