package jdk8feature.functionalinterface;

public class FunctionalTest {

    public FunctionalTest() {
        System.out.println("构造引用");
    }

    public FunctionalTest(String s) {

    }

    public static void print1(String str){
        System.out.println("静态方法");
        System.out.println(str);
    }
    public  void print2(String str){
        System.out.println("实例方法");
        System.out.println(str);
    }
    public static void process(FuncInterface funcInterface){
       funcInterface.print();
    }
    public static void process(FuncInterface funcInterface,String arg){
        funcInterface.print(arg);
    }

    public static void main(String[] args) {
        //静态引用
        FuncInterface funcInterface=FunctionalTest::print1;
        FunctionalTest.process(funcInterface,"999");

        //实例引用
        FuncInterface funcInterface1=new FunctionalTest()::print2;
        FunctionalTest.process(funcInterface1,"666");

        //常规引用-->一种实例引用
        FunctionalTest.process(str->{
            System.out.println("常规的");
            System.out.println(str+"-----------!");
        },"957");

        //构造引用
        FuncInterface funcInterface2=FunctionalTest::new;
        FunctionalTest.process(funcInterface2);

    }
}

@FunctionalInterface
interface FuncInterface{

    void print(String str);

   default void print(){
       System.out.println("默认print");
   };

}
