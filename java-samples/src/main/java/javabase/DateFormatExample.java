package javabase;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author: Administrator
 * @Date: 2023/2/14 15:43
 */

public class DateFormatExample {

    public static void main(String[] args) throws ParseException {

        List<Date> list = new ArrayList<>(500);
        long c1 = System.currentTimeMillis();
        for (int i = 0; i < 900; i++) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date date = sdf.parse("2022-02-" + i % 28 + 1);
            list.add(date);
        }
        long end = System.currentTimeMillis();
        System.out.println(end - c1);
    }

}

