package javabase;

public class TryCatchFinally {
    public static void main(String[] args) throws Throwable {
        Throwable thro=null;
        try {
            try {
                int a=1/0;
            } catch (RuntimeException e) {
                thro = e;

            }finally {
                after(thro);
                throw thro;
            }
        }finally {
            
        }


    }
    public static final String pp(int a){
        try {
            System.out.println(1);
            if (a > 0)
                throw new IllegalArgumentException("");
        } finally {

        }
        return "";
    }

    private static void after(Throwable thro) {
        thro.printStackTrace();
    }
}
