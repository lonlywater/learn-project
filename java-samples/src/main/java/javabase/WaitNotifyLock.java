package javabase;

import java.util.concurrent.locks.ReentrantLock;

public class WaitNotifyLock {
    public static void main(String[] args) {
//        new Wait0().wait0();
        Sleep0 sleep0 = new Sleep0();
        sleep0.multiSleep0();
//        sleep0.sleep0();


    }
}

class Wait0{
    ReentrantLock lock = new ReentrantLock();


    public void wait0(){
        try {
            System.out.println(1);
            //必要的，否则报错
            synchronized (lock){
//                lock.wait(0);
                //wait(0)与wait()，效果等价一直等
                lock.wait(1);
                System.out.println(5);
            }
            System.out.println(2);
            lock.lock();
            System.out.println(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            if (lock.isLocked()){

                lock.unlock();
            }
        }
    }

}
class Sleep0{
    ReentrantLock lock = new ReentrantLock();

    public int sleep0() {
        try {
            Thread.sleep(0);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 重新抢  CPU
     */
    public void multiSleep0(){
        for (int i = 0; i < 10; i++) {
            int finalI = i;
            new Thread(()->{
//                lock.lock();
                System.out.println(finalI+"开始");
                sleep0();
//                lock.unlock();
                System.out.println(finalI+"结束");
            }).start();
        }
    }
}
