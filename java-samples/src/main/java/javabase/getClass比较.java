package javabase;

import java.util.ArrayList;
import java.util.List;

/**
 * 被骗了？
 */
public class getClass比较 {
    public static void main(String[] args) {
        List<Long> longList = new ArrayList<>();
        List<Integer> intList = new ArrayList<>();
        List<Boolean> booleansList = new ArrayList<>();
        Integer i = new Integer(1);

        longList.add((long) 20);
        longList.add((long) 10);

        intList.add(20);
        intList.add(10);

        booleansList.add(true);
        booleansList.add(false);

        System.out.println(longList.getClass() == intList.getClass());
        System.out.println(longList.getClass() == booleansList.getClass());
        System.out.println(longList.getClass());
    }
}
