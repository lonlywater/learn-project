package javabase;

public class Arr {
    public static void main(String[] args) {
        int arr[][]=new int[5][3];
        int a[]= {1,2,3};//ok
        int[] b={1,2,3};//ok
//        int b[2]={1,2};//error
        int c[]=new int[2];//ok
        Integer d[]=new Integer[]{1,2};//ok
        Integer e[]=new Integer[2];//ok
        Integer f[][]=new Integer[2][];//ok
        int g[][]=new int[2][];//ok
//        int g[][]=new int[][2];//err
        System.out.println(f[0]);//null
        System.out.println(g[0]);//null
    }
}
