package javabase;

import java.util.Calendar;

/**
 * @Author: Administrator
 * @Date: 2023/8/1 12:09
 */

public class CalendarDemo {

    public static void main(String[] args) {

        Calendar calendar2= Calendar.getInstance();
        Calendar calendar3= Calendar.getInstance();

        calendar2.set(100000,11,30);
        calendar2.add(Calendar.YEAR,1);
        System.out.println(calendar3==calendar2);
        System.out.println(calendar3.getTime());
        System.out.println(calendar2.getTime());
    }
}

