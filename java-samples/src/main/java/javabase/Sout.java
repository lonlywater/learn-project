package javabase;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class Sout {
    public static void main(String[] args) {
        Period diff = Period.between(LocalDate.parse("2021-09-30"),
                LocalDate.parse("2021-11-30"));

//2021-9-30--->2022-9-29  1年
//2022-9-30-->2023-7-29   10月
//2023-7-30-->2023-7-31   2天
        System.out.println(diff.getYears());
        System.out.println(diff.getMonths());
        System.out.println(diff.getDays());
        System.out.println(diff.isNegative());

        convert();

        LocalDate localDate= LocalDate.of(2020,12,29);
        System.out.println(localDate.format(DateTimeFormatter.ofPattern("yyyy MM dd")));


//        long daysBetween = ChronoUnit.MONTHS.between(LocalDate.parse("2021-08-31"),
//                LocalDate.parse("2021-09-30"));
//        System.out.println(daysBetween);

        Objects.toString("","");
        System.out.println(Calendar.getInstance().getTime().getTime());
        System.out.println(localDate.atStartOfDay().toInstant(ZoneOffset.of("+8")).getEpochSecond()+"秒");
        System.out.println(localDate.atStartOfDay().toInstant(ZoneOffset.of("+8")).toEpochMilli()+"毫秒");
        System.out.println(LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli()+"毫秒");
        System.out.println(new Date(LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli()));


//        /**------------------Date Time----------------------------*/
//        LocalDateTime t1 = LocalDateTime.now();
//        LocalDateTime t2 = LocalDateTime.now().minusMonths(3);
//        long dateTimeDiff = ChronoUnit.MONTHS.between(t2, t1);
//        System.out.println("diff dateTime :" + dateTimeDiff); //diff dateTime : 2
//        /**-------------------------Date----------------------------*/
//        LocalDate t3 = LocalDate.now();
//        LocalDate t4 = LocalDate.now().minusMonths(3);
//        long dateDiff = ChronoUnit.MONTHS.between(t4, t3);
//        System.out.println("diff date :" + dateDiff);//diff date : 3


    }

    public static void convert(){
        System.out.println("Date 转LocalDate："+LocalDateTime.ofInstant(new Date().toInstant(),ZoneId.of("Asia/Shanghai")).toLocalDate());
        System.out.println("Date 转LocalTime："+LocalDateTime.ofInstant(new Date().toInstant(),ZoneId.of("Asia/Shanghai")).toLocalTime());
        System.out.println("LocalDateTime 转 Date ："+Date.from(LocalDateTime.now().toInstant(ZoneOffset.of("+1"))));

    }
}
