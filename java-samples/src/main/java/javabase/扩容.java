package javabase;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

public class 扩容 {
    public static void main(String[] args) {

//        ArrayList();

//        hashmap();

        vector();

    }

    /**
     * 可以指定扩容增长步长
     * 默认扩容是翻倍
     */
    private static void vector() {
        Vector<Object> vector = new Vector<>(1);
        vector.add(2);
        vector.add(2);
    }

    /**
     * 0.resize() 扩容，初始化，和插入以后做判断>threshold,扩容是直接<<1(乘2）
     * 1.DEFAULT_INITIAL_CAPACITY 默认16个，增长因子0.75
     * 2.必须是2的N次幂
     * 3.如果拉链了，则根据oldcap位分为高位链和低位链。=0，低位链，位置不变，=1高位链，位置为原来的位置+oldcap
     * 4.单链node个数=8 最少含有64个槽位
     */
    private static void hashmap() {
        HashMap map = new HashMap();
        for (int i = 0; i < 1000; i++) {
            map.put(i,i);
        }
    }

    /**
     * 1.满了扩容
     * 2.增长上一次的一半
     * 3.最大为整数最大值-8
     * 4.插入之前扩容
     */
    private static void ArrayList() {
        List<Integer> list =new ArrayList<>(10);
        list.add(1);
        list.add(1);
        list.add(1);
        list.add(1);
        list.add(1);
        list.add(1);
        list.add(1);
        list.add(1);
        list.add(1);
        list.add(1);
        list.add(1);//这里扩容
        list.add(1);
    }


    @Test
    public void arraylist(){

        ArrayList();


    }
}
