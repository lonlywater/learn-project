package javabase;

public class StringValOf {
    public static void main(String[] args) {
        String xst="xxxx";

        StringBuffer stringBuffer = new StringBuffer();
        String x=stringBuffer.append("xx").append("xx").toString();
        System.out.println(xst==x);
        System.out.println(xst);
        System.out.println(x);

//        String str="i"与 String str=new String(“i”)一样吗？
        String str="plk";
        String str2=new String("plk");
        System.out.println(str==str2);
        System.out.println(str==str2.intern());
        System.out.println(str2==str2.intern());

    }
}
