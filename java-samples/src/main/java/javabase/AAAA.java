package javabase;

import lombok.Data;
import spring.cyclicRef.B;

import java.util.HashSet;

/**
 * @Data 注解会重写HashCode与equals方法
 */
@Data
public class AAAA {
    public static void main(String[] args) {
        HashSet<AAAA> xx= new HashSet<>();
        AAAA a=new AAAA();
        AAAA b =new AAAA();
        xx.add(a);
        xx.add(b);
        System.out.println(xx.size());
        HashSet<BB> bs=new HashSet<>();

        bs.add(new BB());
        bs.add(new BB());
        System.out.println(bs.size());
    }

    static class BB{

    }
}