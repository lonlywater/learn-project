package pattern.action.mediator.chat;

/**
 * @Author: Administrator
 * @Date: 2023/2/7 21:08
 */

public class Chatroom {

    public void showMsg(User user,String msg){
        System.out.println(user.name+":"+msg);
    }

}

