package pattern.action.mediator.chat;

/**
 * @Author: Administrator
 * @Date: 2023/2/7 21:21
 */

public class Test {
    public static void main(String[] args) {
        Chatroom chatroom=new Chatroom();
        User tom=new User("took",chatroom);
        User nick =new User("nick",chatroom);


        tom.sendMsg("hi,约吗");
        nick.sendMsg("约你妈的单边桥");
    }

}

