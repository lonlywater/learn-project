package pattern.action.mediator.chat;

/**
 * @Author: Administrator
 * @Date: 2023/2/7 21:06
 */

public class User {

    String name;
    Chatroom chatroom;

    public User(String name, Chatroom chatroom) {
        this.name = name;
        this.chatroom = chatroom;
    }

    public void sendMsg(String msg){
        chatroom.showMsg(this,msg);


    }

}

