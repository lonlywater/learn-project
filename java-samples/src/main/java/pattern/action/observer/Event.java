package pattern.action.observer;

/**
 * @Author: Administrator
 * @Date: 2023/2/8 14:53
 */

public class Event {

    String id;
    String contnet;

    public Event(String id, String contnet) {
        this.id = id;
        this.contnet = contnet;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContnet() {
        return contnet;
    }

    public void setContnet(String contnet) {
        this.contnet = contnet;
    }
}

