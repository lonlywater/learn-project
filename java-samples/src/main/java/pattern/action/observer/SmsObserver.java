package pattern.action.observer;

/**
 * @Author: Administrator
 * @Date: 2023/2/8 15:02
 */

public class SmsObserver implements Observer{

    @Override
    public void update(Event event) {

        System.out.println("短信通知:"+event.getId());
    }
}

