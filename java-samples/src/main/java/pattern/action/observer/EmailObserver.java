package pattern.action.observer;

/**
 * @Author: Administrator
 * @Date: 2023/2/8 15:01
 */

public class EmailObserver implements Observer{

    @Override
    public void update(Event event) {
        System.out.println("给用户："+event.getId()+"发送邮件");
    }
}

