package pattern.action.observer;

/**
 * @Author: Administrator
 * @Date: 2023/2/8 15:03
 */

public class Test {
    public static void main(String[] args) {

        Regist regist =new Regist();
        regist.attach(new SmsObserver());
        regist.attach(new EmailObserver());

        regist.notice();
    }

}

