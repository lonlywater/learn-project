package pattern.action.observer;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Administrator
 * @Date: 2023/2/8 14:56
 */

public class Regist implements Subject {
    List<Observer> observers = new ArrayList<>();

    @Override
    public void notice() {
        for (Observer o :observers) {
            o.update(new Event("233用户", "注册"));
        }
    }

    @Override
    public boolean attach(Observer observer) {

        return observers.add(observer);
    }

    @Override
    public boolean clear() {
        observers.clear();
        return true;
    }
}

