package pattern.action.observer;

/**
 * @Author: Administrator
 * @Date: 2023/2/8 14:51
 */

public interface Subject {
    void notice();

    boolean attach(Observer observer);

    boolean clear();

}

