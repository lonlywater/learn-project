package pattern.action.observer;

/**
 * @Author: Administrator
 * @Date: 2023/2/8 14:53
 */

public interface Observer {

    void update(Event event);

}

