package pattern.structure.bridge.message;

/**
 * @Author: Administrator
 * @Date: 2023/2/6 22:32
 */

public class NormalMessage extends AbstractMessage{
    public NormalMessage(Message message) {
        super(message);
    }
}
