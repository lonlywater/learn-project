package pattern.structure.bridge.message;

/**
 * @Author: Administrator
 * @Date: 2023/2/6 22:29
 */

public class EmailMessage implements Message {
    @Override
    public void send(String msg, String to) {
        System.out.println("邮件消息："+msg);
    }
}
