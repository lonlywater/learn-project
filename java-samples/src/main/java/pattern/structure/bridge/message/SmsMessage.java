package pattern.structure.bridge.message;

/**
 * @Author: Administrator
 * @Date: 2023/2/6 22:30
 */

public class SmsMessage implements Message{
    @Override
    public void send(String msg, String to) {
        System.out.println("短信消息：" + msg);
    }
}
