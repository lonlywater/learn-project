package pattern.structure.bridge.message;

/**
 * @Author: Administrator
 * @Date: 2023/2/6 22:32
 */

public abstract class AbstractMessage {
    protected Message message;

    public AbstractMessage(Message message) {
        this.message = message;
    }
    public void send(String message,String to){
        this.message.send(message, to);

    }
}
