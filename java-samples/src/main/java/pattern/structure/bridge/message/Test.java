package pattern.structure.bridge.message;

/**
 * @Author: Administrator
 * @Date: 2023/2/6 22:45
 *
 * 桥接模式：利用组合，而不是继承。将抽象和具体分开（不同的维度）， 使其能够独立变化，扩展
 *
 * 一般而言都是为了将两个维度联系起来
 *
 */

public class Test {
    public static void main(String[] args) {
        Message message=new SmsMessage();
        AbstractMessage abstractMessage=new NormalMessage(message);
        abstractMessage.send("我校要钱","老狗");

        abstractMessage=new UrgentMessage(new EmailMessage());
        abstractMessage.send("真心要钱","爸爸");
    }
}
