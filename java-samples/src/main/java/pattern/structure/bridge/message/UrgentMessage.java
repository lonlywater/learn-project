package pattern.structure.bridge.message;

/**
 * @Author: Administrator
 * @Date: 2023/2/6 22:31
 */

public class UrgentMessage extends AbstractMessage{
    public UrgentMessage(Message message) {
        super(message);
    }

    @Override
    public void send(String message, String to) {
        message="【真急】"+message;
        super.send(message, to);
    }
}
