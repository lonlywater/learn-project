package pattern.structure.bridge.message;

/**
 * @Author: Administrator
 * @Date: 2023/2/6 22:28
 */

public interface Message {
    void send(String msg, String to);
}
