package pattern.structure.composite.transprent;

/**
 * @Author: Administrator
 * @Date: 2023/2/7 11:08
 */

public class CourseConponent {

    private String name;
    private Double price;

    public CourseConponent(String name, Double price) {
        this.name = name;
        this.price = price;
    }

    public boolean add(CourseConponent courseConponent) {
        throw new UnsupportedOperationException("不支持的操作");
    }

    public boolean remove(CourseConponent courseConponent) {
        throw new UnsupportedOperationException("不支持的操作");
    }

    public double getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public void print() {
        throw new UnsupportedOperationException("不支持的操作");
    }

}

