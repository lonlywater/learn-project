package pattern.structure.composite.transprent;

/**
 * @Author: Administrator
 * @Date: 2023/2/7 11:13
 */

public class Course extends CourseConponent{


    public Course(String name, Double price) {
        super(name, price);
    }

    @Override
    public void print() {
        System.out.println("-"+super.getName()+" (￥"+getPrice()+")");
    }
}

