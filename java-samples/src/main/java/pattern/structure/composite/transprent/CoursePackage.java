package pattern.structure.composite.transprent;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Administrator
 * @Date: 2023/2/7 11:18
 */

public class CoursePackage extends CourseConponent{
    private List<CourseConponent> items;
    private Integer level;

    public CoursePackage(String name, Double price, Integer level) {
        super(name, price);
        this.level = level;
        this.items=new ArrayList<>();
    }

    @Override
    public boolean add(CourseConponent courseConponent) {
        return items.add(courseConponent);
    }

    @Override
    public boolean remove(CourseConponent courseConponent) {
        return items.remove(courseConponent);
    }

    @Override
    public void print() {
        System.out.println("+"+getName()+" (￥"+getPrice()+")");
        for (int i = 0; i < items.size(); i++) {
            if (level!=null){
                for (int j = 0; j <level; j++) {
                    System.out.print("   ");
                }
            }
            items.get(i).print();
        }

    }
}

