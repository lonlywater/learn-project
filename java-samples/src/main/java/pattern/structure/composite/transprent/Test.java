package pattern.structure.composite.transprent;

/**
 * @Author: Administrator
 * @Date: 2023/2/7 11:26
 *
 * 组合模式
 *
 * 透明型组合模式：顶层父类或者接口定义好所有操作，对子类完全开放，子类按需复写
 *
 * 违背最少知道
 */

public class Test {

    public static void main(String[] args) {
        Course javabase = new Course("java基础", 2000d);
        Course cpp = new Course("cpp", 1500d);
        Course gol = new Course("golang", 5000d);
        Course design = new Course("设计模式", 2250d);

        CoursePackage cp = new CoursePackage("基础课", 19502d, 2);
        CoursePackage highL = new CoursePackage("进阶课", 19999d, 2);
        CoursePackage cata = new CoursePackage("/", 0d, 1);

        cp.add(javabase);
        cp.add(cpp);
        cp.add(gol);
        highL.add(design);
        cata.add(cp);
        cata.add(highL);
        cata.print();
    }
}

