package pattern.structure.composite.safe;

import pattern.structure.composite.safe.File;
import pattern.structure.composite.safe.Folder;

/**
 * @Author: Administrator
 * @Date: 2023/2/7 12:01
 * <p>
 * 透明板的改进
 * <p>
 * 方法只在需要的类型定义
 * @see pattern.structure.composite.safe.Folder#list()
 */

public class Test {

    public static void main(String[] args) {
        File qq = new File("QQ.exe");
        File wx = new File("wx.exe");
        File tiktok = new File("Tiktok.exe");

        Folder office = new Folder("Office", 2);
        File Word = new File("Word.exe");
        File PPT = new File("PPt.exe");
        office.add(Word);
        office.add(PPT);


        Folder root = new Folder("D://", 1);
        root.add(qq);
        root.add(wx);
        root.add(tiktok);
        root.add(office);
        root.show();

        root.list();

    }
}

