package pattern.structure.composite.safe;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Administrator
 * @Date: 2023/2/7 11:45
 */

public class Folder extends Directory {
    private Integer level;

    private List<Directory> dirs;

    public Folder(String name, Integer level) {
        super(name);
        this.level = level;
        this.dirs = new ArrayList<>();
    }

    public boolean add(Directory directory) {
        return dirs.add(directory);
    }

    public boolean remove(Directory directory) {
        return dirs.remove(directory);
    }

    public void list() {
        for (Directory d : dirs) {
            System.out.println(d.getName());
        }
    }

    @Override
    public void show() {
        System.out.println("+" + getName());
        for (int i = 0; i < dirs.size(); i++) {
            if (level != null) {
                for (int j = 0; j < level; j++) {
                    System.out.print("   ");
                }
            }
            dirs.get(i).show();
        }
    }
}

