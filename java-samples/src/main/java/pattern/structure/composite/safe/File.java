package pattern.structure.composite.safe;

/**
 * @Author: Administrator
 * @Date: 2023/2/7 11:45
 */

public class File extends Directory {


    public File(String name) {
        super(name);
    }

    public void show() {
        System.out.println("-" + getName());
    }
}

