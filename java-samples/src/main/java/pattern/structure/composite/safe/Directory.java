package pattern.structure.composite.safe;

/**
 * @Author: Administrator
 * @Date: 2023/2/7 11:44
 */

public abstract class Directory {
    private String name;

    public Directory(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract void show();
}

