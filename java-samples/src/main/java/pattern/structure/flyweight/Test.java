package pattern.structure.flyweight;

import javabase.Sout;

/**
 * @Author: Administrator
 * @Date: 2023/2/7 10:14
 * 享元  对象的重复创建开销大，并且可以重复利用
 * <p>
 * 池化技术都是
 */

public class Test {

    public static void main(String[] args) {
        //1.字符串常量池
        String s1 = "hello";
        String s2 = "hel" + "lo";

        String s3 = "hel";
        String s4 = "lo";
        String s5 = s3 + s4;
        String s6 = new String("hello");
        String s7 = new String("hello").intern();

        System.out.println(s1 == s2);//true
        System.out.println(s1 == s5);//false
        System.out.println(s1 == s6);//false
        System.out.println(s1 == s7);//true


        //2.连接池类的


        //Integer/Long

        Integer i1 = 127;
        Integer i2 = Integer.valueOf(127);

        Integer i3 = 128;
        Integer i4 = Integer.valueOf(128);

        System.out.println(i1 == i2);//true
        System.out.println(i3 == i4);//false

        Long l1 = 127l;
        Long l2 = Long.valueOf(127);

        Long l3 = 128l;
        Long l4 = Long.valueOf(128);

        System.out.println(l1 == l2);//true
        System.out.println(l3 == l4);//false


    }

}

