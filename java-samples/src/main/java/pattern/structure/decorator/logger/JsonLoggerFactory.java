package pattern.structure.decorator.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Author: Administrator
 * @Date: 2023/2/7 16:09
 */

public class JsonLoggerFactory {

    public static JsonLogger getLogger(Class cls) {
        Logger logger = LoggerFactory.getLogger(cls);
        return new JsonLogger(logger);
    }

}

