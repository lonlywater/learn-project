package pattern.structure.decorator.logger;

import lombok.SneakyThrows;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;

/**
 * @Author: Administrator
 * @Date: 2023/2/7 16:03
 */

public class JsonLogger extends LoggerDecorator {

    public JsonLogger(Logger logger) {
        super(logger);
    }


    @Override
    public void info(String s) {
        JSONObject jsonObject = newJson();
        try {
            jsonObject.put("INFO", s);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        logger.info(jsonObject.toString());
    }

    @Override
    public void error(String s) {
        JSONObject jsonObject = newJson();
        try {
            jsonObject.put("error", s);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        logger.error(jsonObject.toString());
    }

    public JSONObject newJson() {
        return new JSONObject();
    }
}

