package pattern.structure.decorator.logger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Author: Administrator
 * @Date: 2023/2/7 15:58
 */

public class Test {
    public static final Logger logger = JsonLoggerFactory.getLogger(Test.class);

    public static void main(String[] args) {
        logger.info("如何打印JSON");
        logger.error("hjkhkj");
    }

}

