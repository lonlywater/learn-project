package pattern.structure.decorator.pancake;

/**
 * @Author: Administrator
 * @Date: 2023/2/7 15:51
 */

public class EggDecorator extends PancakeDecorator{

    public EggDecorator(Pancake pancake) {
        super(pancake);
    }

    @Override
    public String info() {
        return super.info()+"+1个鸡蛋";
    }

    @Override
    public int price() {
        return super.price()+2;
    }
}

