package pattern.structure.decorator.pancake;

/**
 * @Author: Administrator
 * @Date: 2023/2/7 15:50
 */

public class BasePancake extends Pancake{

    @Override
    public String info() {
        return "煎饼";
    }

    @Override
    public int price() {
        return 15;
    }
}

