package pattern.structure.decorator.pancake;

/**
 * @Author: Administrator
 * @Date: 2023/2/7 15:54
 */

public class Test {

    public static void main(String[] args) {
        Pancake pancake=new BasePancake();

        //加鸡蛋
        pancake=new EggDecorator(pancake);

        //加生菜
        pancake=new LettuceDecorator(pancake);

        System.out.println(pancake);
    }
}

