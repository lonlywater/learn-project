package pattern.structure.decorator.pancake;

/**
 * @Author: Administrator
 * @Date: 2023/2/7 15:46
 */

public abstract class Pancake {

    public abstract String info();

    public abstract int price();

    @Override
    public String toString() {
        return info()+"  总价："+price();
    }
}

