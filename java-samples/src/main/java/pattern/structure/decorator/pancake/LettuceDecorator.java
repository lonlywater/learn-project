package pattern.structure.decorator.pancake;

/**
 * @Author: Administrator
 * @Date: 2023/2/7 15:53
 */

public class LettuceDecorator extends PancakeDecorator{

    @Override
    public String info() {
        return super.info()+"+1份生菜";
    }

    @Override
    public int price() {
        return super.price()+1;
    }

    public LettuceDecorator(Pancake pancake) {
        super(pancake);
    }
}

