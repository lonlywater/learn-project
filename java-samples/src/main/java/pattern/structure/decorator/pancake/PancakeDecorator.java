package pattern.structure.decorator.pancake;

/**
 * @Author: Administrator
 * @Date: 2023/2/7 15:48
 */

public class PancakeDecorator extends Pancake{

    protected Pancake pancake;

    public PancakeDecorator(Pancake pancake) {
        this.pancake = pancake;
    }

    @Override
    public String info() {
        return pancake.info();
    }

    @Override
    public int price() {
        return pancake.price();
    }
}

