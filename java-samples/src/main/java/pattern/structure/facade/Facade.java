package pattern.structure.facade;

/**
 * @Author: Administrator
 * @Date: 2023/2/7 15:00
 */

public class Facade {

    SubsystemA a=new SubsystemA();
    SubsystemB b=new SubsystemB();
    SubsystemC c=new SubsystemC();

    public void doA(){
        a.doA();
    }
    public void doB(){
        b.doB();
    }
    public void doC(){
        c.doC();
    }

}

