package pattern.structure.proxy.dynamic;

import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;


import java.lang.reflect.Method;

public class CGLibProxy implements MethodInterceptor {

    public static void main(String[] args) {
        CGLibProxy c = new CGLibProxy();
        Zhangsan p = c.getProxy(Zhangsan.class);
        p.findLove();
    }

    public <T> T getProxy(Class<T> cls) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(cls);
        enhancer.setCallback(this);
        return (T) enhancer.create();
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        System.out.println("代理动手了");
        return methodProxy.invokeSuper(o, objects);
    }
}
