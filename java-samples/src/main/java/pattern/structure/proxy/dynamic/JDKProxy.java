package pattern.structure.proxy.dynamic;

import pattern.structure.proxy.Person;
import pattern.structure.proxy.staticp.LIsi;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class JDKProxy implements InvocationHandler {

    private Person target;
    public JDKProxy(Person person){
        this.target=person;
    }


    public static void main(String[] args) {
        Person p= (Person) Proxy.newProxyInstance(JDKProxy.class.getClassLoader(),new Class[]{Person.class}
        ,new JDKProxy(new Zhangsan()));
        p.findLove();


        Person p1= (Person) Proxy.newProxyInstance(JDKProxy.class.getClassLoader(),new Class[]{Person.class}
        ,new JDKProxy(new LIsi()));
        p1.findLove();

    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("代理动手了");

        return method.invoke(this.target,args);
    }
}
