package pattern.structure.proxy;

public interface Person {
    void findLove();
}
