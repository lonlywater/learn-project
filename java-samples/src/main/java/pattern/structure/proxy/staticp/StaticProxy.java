package pattern.structure.proxy.staticp;

import pattern.structure.proxy.Person;

public class StaticProxy implements Person{

    private Person target;

    private StaticProxy(Person p) {
        this.target=p;
    }

    public static Person getInstance(Person p){
        return  new StaticProxy(p);
    }

    @Override
    public void findLove() {
        System.out.println("代理物色中");
        target.findLove();

    }

    public static void main(String[] args) {
        getInstance(new Zhangsan()).findLove();
        getInstance(new LIsi()).findLove();
    }
}
