package pattern.structure.proxy.staticp;

import pattern.structure.proxy.Person;

public class Zhangsan implements Person {
    @Override
    public void findLove() {
        System.out.println("有腿就好");
    }
}