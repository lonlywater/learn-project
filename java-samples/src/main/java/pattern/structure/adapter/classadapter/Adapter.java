package pattern.structure.adapter.classadapter;

/**
 * @Author: Administrator
 * @Date: 2023/2/2 20:28
 */

public class Adapter extends AC220 implements DC5{
    @Override
    public int output5V() {
        System.out.println("电源适配...");
        return super.output()/44;
    }
}
