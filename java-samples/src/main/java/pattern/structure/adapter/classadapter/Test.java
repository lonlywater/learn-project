package pattern.structure.adapter.classadapter;

/**
 * @Author: Administrator
 * @Date: 2023/2/2 20:29
 * 220v需要适配为5v
 *
 * 类型适配：通过继承获得源目标的能力，并实现目标功能的接口，而获得目标能力
 */

public class Test {
    public static void main(String[] args) {
        DC5 dc5 = new Adapter();
        System.out.println(dc5.output5V());
    }
}
