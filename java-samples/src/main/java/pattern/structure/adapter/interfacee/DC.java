package pattern.structure.adapter.interfacee;

/**
 * @Author: Administrator
 * @Date: 2023/2/2 20:26
 */

public interface DC {
    int output5V();

    int output22V();

    int output11V();


}
