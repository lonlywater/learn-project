package pattern.structure.adapter.interfacee;

/**
 * @Author: Administrator
 * @Date: 2023/2/2 20:29
 * 220v需要适配为5v
 *
 * 接口适配：定义一个接口，包含一类需要适配的功能
 *
 * 无论对象适配还是类型适配都可以实现
 *
 * 不符合接口隔离。单一职责
 */

public class Test {
    public static void main(String[] args) {
        DC dc5 = new Adapter(new AC220());
        System.out.println(dc5.output5V());
        System.out.println(dc5.output22V());
        System.out.println(dc5.output11V());


    }
}
