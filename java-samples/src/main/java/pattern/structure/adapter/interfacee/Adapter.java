package pattern.structure.adapter.interfacee;

/**
 * @Author: Administrator
 * @Date: 2023/2/2 20:28
 */

public class Adapter  implements DC {
    
    private AC220 ac220;

    public Adapter(AC220 ac220) {
        this.ac220 = ac220;
    }

    @Override
    public int output5V() {
        System.out.println("电源适配...");
        return this.ac220.output() / 44;
    }

    @Override
    public int output22V() {
        return this.ac220.output() / 10;
    }

    @Override
    public int output11V() {
        return this.ac220.output() / 20;
    }
}
