package pattern.structure.adapter.objectadapter;

/**
 * @Author: Administrator
 * @Date: 2023/2/2 20:33
 */

public class Adapter implements DC5{
    private AC220 ac220;
    public Adapter(AC220 ac220){
        this.ac220=ac220;
    }
    @Override
    public int output5V() {
        System.out.println("适配中...");
        return this.ac220.output()/44;
    }
}
