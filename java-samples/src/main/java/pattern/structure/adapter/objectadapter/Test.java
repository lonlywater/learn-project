package pattern.structure.adapter.objectadapter;



/**
 * @Author: Administrator
 * @Date: 2023/2/2 20:29
 * 220v需要适配为5v
 *
 * 不继承源目标，通过组合复用的手段来适配
 */

public class Test {
    public static void main(String[] args) {
        DC5 dc5 = new Adapter(new AC220());
        System.out.println(dc5.output5V());
    }
}
