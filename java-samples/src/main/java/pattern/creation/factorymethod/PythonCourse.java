package pattern.creation.factorymethod;

/**
 * @Author: Administrator
 * @Date: 2023/2/1 20:27
 */

public class PythonCourse implements Course {
    @Override
    public void learn() {
        System.out.println("学习python");
    }
}
