package pattern.creation.factorymethod;

/**
 * @Author: Administrator
 * @Date: 2023/2/1 20:50
 */

public interface CourseFactory {
    Course create();
}
