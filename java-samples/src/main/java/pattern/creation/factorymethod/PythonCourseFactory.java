package pattern.creation.factorymethod;

/**
 * @Author: Administrator
 * @Date: 2023/2/1 20:51
 */

public class PythonCourseFactory implements CourseFactory {
    @Override
    public Course create() {
        return new PythonCourse();
    }
}
