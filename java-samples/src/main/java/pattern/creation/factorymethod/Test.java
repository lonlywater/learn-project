package pattern.creation.factorymethod;

/**
 * @Author: Administrator
 * @Date: 2023/2/1 20:53
 *
 * 工厂方法模式
 * 关注不同类的创建过程的分离    解决简单工厂维护爆炸的问题
 *
 * 解决创建对象时需要大量重复代码问题
 *
 * 符合开闭原则、单一职责
 *
 * 工厂类数量多
 *
 */

public class Test {
    public static void main(String[] args) {

        Course course= new JavaCourseFactory().create();
        course.learn();


        Course course1= new PythonCourseFactory().create();
        course1.learn();


    }
}
