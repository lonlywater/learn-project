package pattern.creation.singleton;

/**
 * @Author: Administrator
 * @Date: 2023/5/12 12:49
 */

public class Test {

    static class Single{
        private static final Single instance= new Single();
        private Single(){

        }

        public static Single getInstance(){
            return instance;

        }
    }

    static class Single2{

        private Single2(){

        }

        public static Single2 getInstance(){
            return Single2Holder.instance;

        }
        static class Single2Holder
        {
            private static final Single2 instance= new Single2();
        }
    }
}


