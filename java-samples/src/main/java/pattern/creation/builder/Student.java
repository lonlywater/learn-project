package pattern.creation.builder;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @Author: Administrator
 * @Date: 2023/5/12 12:39
 */

@Builder
@Getter
@Setter
@ToString()
public class Student {
    private int age;
    String name;
    String hobby;
    String gender;
}

