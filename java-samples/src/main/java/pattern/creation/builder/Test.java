package pattern.creation.builder;

/**
 * @Author: Administrator
 * @Date: 2023/5/12 12:41
 * @see lombok.Builder 就是一种实现
 */

public class Test {

    public static void main(String[] args) {
        System.out.println(Student.builder().age(11).hobby("唱跳rap篮球").build());
        System.out.println(Student.builder().name("小丽").gender("男").age(999).build());
    }
}

