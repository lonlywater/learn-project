package pattern.creation.abstractfactory;

/**
 * @Author: Administrator
 * @Date: 2023/2/1 21:58
 */

public class JavaCourseFactory implements Factory{
    @Override
    public Video createVideo() {
        return new JavaVideo();
    }

    @Override
    public Course creatCrouse() {
        return new JavaCourse();
    }

    @Override
    public Note createNote() {
        return new JavaNote();
    }
}
