package pattern.creation.abstractfactory;

/**
 * @Author: Administrator
 * @Date: 2023/2/1 22:00
 */

public class PythonVideo implements Video{
    @Override
    public void record() {
        System.out.println("录制Python");
    }
}
