package pattern.creation.abstractfactory;

/**
 * @Author: Administrator
 * @Date: 2023/2/1 21:58
 */

public class PythonCourseFactory implements Factory{
    @Override
    public Video createVideo() {
        return new PythonVideo();
    }

    @Override
    public Course creatCrouse() {
        return new PythonCourse();
    }

    @Override
    public Note createNote() {
        return new PythonNote();
    }
}
