package pattern.creation.abstractfactory;

/**
 * @Author: Administrator
 * @Date: 2023/2/1 22:02
 */

public class PythonNote implements Note{
    @Override
    public void edit() {
        System.out.println("Python 笔记");
    }
}
