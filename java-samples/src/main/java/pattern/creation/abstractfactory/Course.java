package pattern.creation.abstractfactory;

/**
 * @Author: Administrator
 * @Date: 2023/2/1 20:27
 */

public interface Course {
    void learn();
}
