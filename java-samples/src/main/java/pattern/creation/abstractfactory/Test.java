package pattern.creation.abstractfactory;

/**
 * @Author: Administrator
 * @Date: 2023/2/1 22:04
 *
 * 抽象工厂模式  定义一类 / 一族对象的创建接口
 *
 * 将一族 对象关联在一起
 * 工厂抽象定义了所有的类的创建接口，，，不利于扩展，扩展需要改顶层接口
 *
 */

public class Test {

    public static void main(String[] args) {
        Factory javaCourseFactory=new JavaCourseFactory();
        javaCourseFactory.creatCrouse().learn();
        javaCourseFactory.createNote().edit();
        javaCourseFactory.createVideo().record();

        Factory py=new PythonCourseFactory();
        py.createVideo().record();
        py.createNote().edit();
        py.creatCrouse().learn();

    }
}
