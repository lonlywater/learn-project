package pattern.creation.abstractfactory;

/**
 * @Author: Administrator
 * @Date: 2023/2/1 21:18
 */

public interface Note {

    void edit();
}
