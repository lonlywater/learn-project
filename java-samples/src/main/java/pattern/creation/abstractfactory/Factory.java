package pattern.creation.abstractfactory;

/**
 * @Author: Administrator
 * @Date: 2023/2/1 21:16
 */

public interface Factory {
     Video createVideo();
     Course creatCrouse();
     Note createNote();
}
