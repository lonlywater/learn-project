package pattern.creation.abstractfactory;

/**
 * @Author: Administrator
 * @Date: 2023/2/1 21:17
 */

public interface Video {
    void record();
}
