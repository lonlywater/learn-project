package pattern.creation.simpleFactory;

/**
 * @Author: Administrator
 * @Date: 2023/2/1 20:31
 */

public class  CourseFactory {


    public static final Course createCourse(Class<? extends Course> cls) {
        if (cls != null) {
            try {
                return cls.newInstance();
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


    public static final Course createCourse(String clsName) {
        if (clsName != null) {
            switch (clsName){
                case "java":
                    return new JavaCourse();

                case "python":
                    return new PythonCourse();
                case "cpp":
                    return new CPPCourse();
                default:
                    return null;
            }
        }
        return null;
    }
}
