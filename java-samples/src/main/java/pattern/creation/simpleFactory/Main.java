package pattern.creation.simpleFactory;

/**
 * @Author: Administrator
 * @Date: 2023/2/1 20:30
 *
 * 简单工厂：将对象的创建与使用分离。降低了耦合性。
 * 符合《最少知道》
 * 不符合《开闭》《单一》
 */

public class Main {

    public static void main(String[] args) {

        Course c=CourseFactory.createCourse(CPPCourse.class);
        c.learn();

    }


}
