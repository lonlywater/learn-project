##动态代理demo
采用两种方式实现动态代理：  
a. JDKProxy  
b. Cjlicb


###Jdk proxy
jdk proxy实现动态代理是基于接口的：java.lang.reflect.Proxy.newProxyInstance
![img.png](img.png)

###cjlib
cjLib实现动态代理在Spring中是使用在没有接口的情况下，但是CJLib本身是支持对接口代理的，而不仅仅是继承
![img_1.png](img_1.png)
![img_2.png](img_2.png)