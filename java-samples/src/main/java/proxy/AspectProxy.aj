package proxy;

public aspect AspectProxy {
    void around():call(public void Action2.hello()){
        System.out.println("AspectProxy开始 ...");
        proceed();
        System.out.println("AspectProxy结束 ...");
    }

}
