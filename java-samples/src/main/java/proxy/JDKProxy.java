package proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class JDKProxy {
    public static void main(String[] args) {
        jdkProxy();
    }

    /**
     * jdk，实现与代理对象一样的接口，
     * 然后代理方法中去调用 目标对象的方法
     */
    private static void jdkProxy() {
        MyProxyActions target = new Action1();
        MyProxyActions proxy = (MyProxyActions) Proxy.newProxyInstance(JDKProxy.class.getClassLoader(), new Class[]{MyProxyActions.class}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                System.out.println("before:"+method.getName());
                Object result=method.invoke(target,args);
                System.out.println("after:"+method.getName());
                return result;
            }
        });
        proxy.hello();

    }
}

