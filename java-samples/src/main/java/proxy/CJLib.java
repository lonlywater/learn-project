package proxy;

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

public class CJLib {
    public static void main(String[] args) {
        Action1 action1 = new Action1();
        MyProxyActions object = (MyProxyActions) Enhancer.create(Action1.class, new MethodInterceptor() {
            @Override
            public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                System.out.println("1");
                Object ob=method.invoke(action1,objects);
                System.out.println("2");
                return ob;
            }
        });
        object.hello();

        //cjlib实现接口代理
        MyProxyActions proxyActions2= (MyProxyActions) Enhancer.create(Object.class, new Class[]{MyProxyActions.class}, new MethodInterceptor() {
            @Override
            public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
                System.out.println("11");
                Object ob=method.invoke(action1,objects);
                System.out.println("22");
                return ob;
            }
        });
        proxyActions2.hello();


    }
}
